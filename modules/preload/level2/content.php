<?php

	//------------------------------------------------------------------------------
	//|                                                                            |
	//|            Content Management System SiMan CMS                             |
	//|                                                                            |
	//------------------------------------------------------------------------------

	use SM\SM;

	if (!defined("SIMAN_DEFINED"))
		exit('Hacking attempt!');

	if (SM::User()->Level()>=intval(sm_settings('content_editor_level')))
		include_once(SM::ModulesPath('preload/level_inc/content.php'));

