<?php

	//------------------------------------------------------------------------------
	//|                                                                            |
	//|            Content Management System SiMan CMS                             |
	//|                                                                            |
	//------------------------------------------------------------------------------

	use SM\SM;

	if (!defined("SIMAN_DEFINED"))
		exit('Hacking attempt!');

	function siman_block_items_news($blockinfo)
		{
			global $lang;
			$sql = "SELECT * FROM ".sm_table_prefix()."categories_news";
			$result = execsql($sql);
			$i = 0;
			$res=[];
			while ($row = database_fetch_object($result))
				{
					$res[$i]['caption'] = ' - '.$lang['show_on_category'].': '.$row->title_category;
					$res[$i]['value'] = 'news|'.$row->id_category;
					if (
						!empty($blockinfo['show_on_module_block'])
						&& !empty($blockinfo['show_on_ctg_block'])
						&& sm_strcmp($blockinfo['show_on_module_block'], 'news') == 0
						&& $blockinfo['show_on_ctg_block'] == $row->id_category
					)
						$res[$i]['selected'] = 1;
					$i++;
				}
			return $res;
		}
	
	if (SM::User()->Level()>=SM::Settings('news_editor_level')->AsInt())
		include_once(SM::ModulesPath('preload/level_inc/news.php'));
	