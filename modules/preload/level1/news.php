<?php

	//------------------------------------------------------------------------------
	//|                                                                            |
	//|            Content Management System SiMan CMS                             |
	//|                                                                            |
	//------------------------------------------------------------------------------

	use SM\SM;

	if (!defined("SIMAN_DEFINED"))
		exit('Hacking attempt!');

	if (SM::User()->Level()>=SM::Settings('news_editor_level')->AsInt())
		include_once(SM::ModulesPath('preload/level_inc/news.php'));

