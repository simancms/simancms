<?php

	//------------------------------------------------------------------------------
	//|            Content Management System SiMan CMS                             |
	//|              http://simancms.apserver.org.ua                               |
	//------------------------------------------------------------------------------

	//==============================================================================
	//#revision 2024-07-14
	//==============================================================================

	use SM\Access\SMAccess;
	use SM\Common\Redirect;
	use SM\SM;
	use SM\UI\Buttons;
	use SM\UI\Form;
	use SM\UI\Grid;
	use SM\UI\Navigation;
	use SM\UI\UI;

	if (!defined("SIMAN_DEFINED"))
		exit('Hacking attempt!');

	if (!defined("MENU_ADMINPART_FUNCTIONS_DEFINED"))
		{
			function siman_delete_menu_line($line_id)
				{
					$result = execsql("SELECT id_ml FROM ".sm_table_prefix()."menu_lines WHERE submenu_from=".intval($line_id));
					while ($row = database_fetch_assoc($result))
						{
							siman_delete_menu_line($row['id_ml']);
						}
					if (intval(sm_settings('menuitems_use_image'))==1)
						{
							if (file_exists(SM::FilesPath('img/menuitem'.intval($line_id).'.jpg')))
								unlink(SM::FilesPath('img/menuitem'.intval($line_id).'.jpg'));
						}
					execsql("DELETE FROM ".sm_table_prefix()."menu_lines WHERE id_ml=".intval($line_id));
				}

			define("MENU_ADMINPART_FUNCTIONS_DEFINED", 1);
		}


	if (SM::isAdministrator())
		{
			sm_on_action('admin', function ()
				{
					add_path_modules();
					add_path_current();
					sm_title(sm_lang('control_panel').' - '.sm_lang('module_menu.module_menu_name'));
					$ui = new UI();
					$nav=new Navigation();
					$nav->AddItem(sm_lang('list_menus'), 'index.php?m=menu&d=listmenu');
					$nav->AddItem(sm_lang('add_menu'), 'index.php?m=menu&d=add');
					$ui->Add($nav);
					$ui->Output(true);
				});

			sm_on_action('addouter', function ()
				{
					sm_title(sm_lang('module_menu.add_menu_line'));
					$ui = new UI();
					$f = new Form('index.php?m=menu&d=prepareaddline&returnto='.SM::GET('returnto')->UrlencodedString());
					$f->AddText('p_caption', sm_lang('caption'))
						->SetFocus();
					$f->AddText('p_url', sm_lang('url'));
					$values=[];
					$labels=[];
					$q=new TQuery(sm_table_prefix().'menus');
					$q->OrderBy('if (id_menu_m=1, 1, 0), caption_m');
					$q->Select();
					for ($i = 0; $i < $q->Count(); $i++)
						{
							$values[]=$q->items[$i]['id_menu_m'].'|0';
							$labels[]='['.$q->items[$i]['caption_m'].']';
							$lines = siman_load_menu($q->items[$i]['id_menu_m']);
							foreach ($lines as $line)
								{
									$prefix=str_repeat(' - ', $line['level']);
									$values[]=$line['add_param'];
									$labels[]=$prefix.$line['caption'];
								}
						}
					$f->AddSelect('p_mainmenu', sm_lang('module_menu.add_to_menu'), $values, $labels);
					$f->LoadValuesArray(sm_postvars());
					$f->LoadValuesArray(sm_getvars());
					$ui->AddForm($f);
					$ui->Output(true);
				});

			sm_on_action('postadd-dialog', function ()
				{
					sm_title(sm_lang('module_menu.add_menu_line'));
					$ui=new UI();
					$ui->p(sm_lang('you_want_add_line'));
					$b=new Buttons();
					$b->Button(sm_lang('no'), 'index.php?m=menu&d=listmenu');
					$b->Button(sm_lang('yes'), 'index.php?m=menu&d=addline&mid='.SM::GET('id')->AsInt());
					$ui->Add($b);
					$ui->Output(true);
				});

			sm_on_action('postadd', function ()
				{
					$title=SM::POST('p_caption')->AsString();
					if (empty($title))
						{
							SM::Errors()->AddError(sm_lang('messages.fill_required_fields'));
							sm_set_action('add');
						}
					else
						{
							$id_menu=TQuery::ForTable(sm_table_prefix().'menus')
										   ->AddString('caption_m', $title)
										   ->Insert();
							if (sm_settings('menus_use_image')>0)
								{
									siman_upload_image($id_menu, 'menu');
								}
							Redirect::Now('index.php?m=menu&d=postadd-dialog&id='.$id_menu);
						}
				});

			sm_on_action('add', function ()
				{
					add_path_modules();
					add_path(sm_lang('module_menu.module_menu_name'), 'index.php?m=menu&d=admin');
					add_path_current();
					sm_title(sm_lang('add_menu'));
					$ui = new UI();
					SM::Errors()->DisplayUIErrors($ui);
					$f = new Form('index.php?m='.sm_current_module().'&d=postadd');
					$f->AddText('p_caption', sm_lang('caption'), true)->SetFocus();
					if (intval(sm_settings('menus_use_image'))>0)
						$f->AddFile('userfile', sm_lang('common.image'));
					$ui->Add($f);
					$ui->Output(true);
				});

			if (sm_action('postdeleteline'))
				{
					siman_delete_menu_line(SM::GET('lid')->AsInt());
					Redirect::Now('index.php?m=menu&d=listlines&mid='.SM::GET('mid')->AsInt());
				}
			if (sm_action('postaddouter'))
				{
					SM::GET('mid')->SetValue(SM::POST('p_mid')->AsString());
					$lposition = 0;
					$m["mode"] = 'postaddline';
				}
			if (sm_action('postaddline'))
				{
					$lcaption = sm_postvars("p_caption");
					$menu_id = SM::GET('mid')->AsInt();
					$lurl = sm_postvars("p_url");
					$submenu_from = intval(sm_postvars("p_sub"));
					$lposition = intval(sm_postvars("p_position"));
					$alt_ml = dbescape(sm_postvars("p_alt"));
					$newpage_ml = intval(sm_postvars("p_newpage"));
					if ($lposition == 0)
						{
							$sql = "SELECT max(position) FROM ".sm_table_prefix()."menu_lines WHERE id_menu_ml=".$menu_id." AND submenu_from=".$submenu_from;
							$lposition = 1;
							$result = execsql($sql);
							while ($row = database_fetch_row($result))
								{
									$lposition = $row[0] + 1;
								}
						}
					else
						{
							$sql = "UPDATE ".sm_table_prefix()."menu_lines SET position=position+1 WHERE position >= ".$lposition." AND id_menu_ml=".$menu_id." AND submenu_from=".$submenu_from;
							$result = execsql($sql);
						}
					$sql = "INSERT INTO ".sm_table_prefix()."menu_lines (id_menu_ml, submenu_from, url, caption_ml, position, alt_ml, newpage_ml) VALUES ('".dbescape($menu_id)."', '".dbescape($submenu_from)."', '".dbescape($lurl)."', '".dbescape($lcaption)."', '".dbescape($lposition)."', '".dbescape($alt_ml)."', '".dbescape($newpage_ml)."')";
					$id_ml = insertsql($sql);
					if (sm_settings('menuitems_use_image') == 1)
						{
							siman_upload_image($id_ml, 'menuitem');
						}
					if (!empty(sm_getvars('returnto')))
						Redirect::Now(sm_getvars('returnto'));
					else
						Redirect::Now('index.php?m=menu&d=listlines&mid='.$menu_id);
				}

			sm_on_action('addline', function ()
				{
					sm_title(sm_lang('module_menu.add_menu_line'));
					$menu_id = SM::GET('mid')->AsInt();
					$rmenu=[];
					$menu=(new SMNavigation($rmenu))->LoadFromDB($menu_id);
					SMAccess::Error404IfEmpty($menu->HasMenuID());

					$values=[$menu_id.'|0'];
					$labels=['['.sm_lang('module_menu.menu_root').']'];

					foreach ($rmenu as $item)
						{
							$values[]=$item['add_param'];
							$labels[]=str_repeat('- ', $item['level']).$item['caption'];
						}

					$ui=new UI();
					$f=new Form('index.php?m=menu&d=prepareaddline');
					$f->AddText('p_caption', sm_lang('caption'), true)->SetFocus();
					$f->AddText('p_url', sm_lang('url'));
					$f->AddSelect('p_mainmenu', sm_lang('module_menu.add_to_menu'), $values, $labels, true);
					$f->SaveButton(sm_lang('next'));
					$ui->Add($f);
					$ui->Output(true);
				});

			sm_on_action('prepareaddline', function ()
				{
					$tmp=explode('|', SM::POST('p_mainmenu')->AsString());
					if (count($tmp)!==2)
						SMAccess::EnforceError404();
					$menu_id=$tmp[0];
					$submenu_from=$tmp[1];

					$list=(new TQuery(sm_table_prefix().'menu_lines'))
						->AddNumeric('id_menu_ml', $menu_id)
						->AddNumeric('submenu_from', $submenu_from)
						->OrderBy('position')
						->Select();
					$values=[];
					$labels=[];
					foreach ($list->items as $item)
						{
							$values[]=$item['pos'];
							$labels[]=sm_lang('before').': '.$item['caption_ml'];
						}
					$values[]=0;
					$labels[]=sm_lang('last');

					$ui=new UI();
					sm_title(sm_lang('module_menu.add_menu_line'));
					$f=new Form('index.php?m=menu&d=postaddline&mid='.$menu_id.'&returnto='.SM::GET('returnto')->UrlencodedString());
					$f->AddText('p_caption', sm_lang('caption'), true)->SetFocus();
					$f->AddText('p_url', sm_lang('url'));
					$f->AddHidden('p_sub', $submenu_from);
					$f->AddText('p_alt', sm_lang('common.alt_text'));
					$f->AddCheckbox('p_newpage', sm_lang('module_menu.open_in_new_page'));
					$f->AddSelect('p_position', sm_lang('module_menu.position'), $values, $labels, true)
						->WithValue(0);
					$f->LoadValuesArray(SM::Requests()->POSTAsArray());
					$ui->Add($f);
					$ui->Output(true);
				});
			
			sm_on_action('posteditline', function ()
				{
					$menu_id = SM::GET('mid')->AsInt();
					$menuline_id = SM::GET('lid')->AsInt();
					$submenu_from = SM::GET('sid')->AsInt();

					$lposition=SM::POST('p_position')->AsInt();

					if (SM::POST('caption_ml')->isEmpty())
						{
							SM::Errors()->AddFillRequiredFieldsError();
							sm_set_action('editline');
							return;
						}

					$all_items=(new TQuery(sm_table_prefix().'menu_lines'))
						->AddNumeric('id_menu_ml', $menu_id)
						->AddNumeric('submenu_from', $submenu_from)
						->OrderBy('position')
						->Select()
						->items;

					if ($lposition == -1)
						{
							if (count($all_items) > 0)
								{
									$lposition = intval($all_items[count($all_items) - 1]['position']) + 1;
								}
							else
								{
									$lposition = 1;
								}
						}
					elseif (!empty($lposition))
						{
							foreach ($all_items as $item)
								{
									if ($item['position'] >= $lposition)
										{
											(new TQuery(sm_table_prefix().'menu_lines'))
												->AddNumeric('position', $item['position'] + 1)
												->Update('id_ml', $item['id_ml']);
										}
								}
						}

					$upd=new TQuery(sm_table_prefix().'menu_lines');
					$upd->AddString('caption_ml', SM::POST('caption_ml')->AsString());
					$upd->AddString('url', SM::POST('url')->AsString());
					$upd->AddNumeric('partial_select', SM::POST('partial_select')->AsInt());
					$upd->AddNumeric('newpage_ml', SM::POST('newpage_ml')->AsInt());
					$upd->AddString('alt_ml', SM::POST('alt_ml')->AsString());
					$upd->AddString('attr_ml', SM::POST('attr_ml')->AsString());
					if (!empty($lposition))
						$upd->AddNumeric('position', $lposition);
					$upd->Update('id_ml', $menuline_id);

					if (SM::Settings('menuitems_use_image')->AsBool())
						siman_upload_image($menuline_id, 'menuitem');

					sm_notify(sm_lang('edit_menu_line_successful'));
					Redirect::Now('index.php?m=menu&d=listlines&mid='.$menu_id);
				});


			sm_on_action('editline', function ()
				{
					$menu_id = SM::GET('mid')->AsInt();
					$menuline_id = SM::GET('lid')->AsInt();
					$submenu_from = SM::GET('sid')->AsInt();

					$list=(new TQuery(sm_table_prefix().'menu_lines'))
						->AddNumeric('id_menu_ml', $menu_id)
						->AddNumeric('submenu_from', $submenu_from)
						->OrderBy('position')
						->Select();

					$selected_item=[];
					$values=[0];
					$labels=[sm_lang('do_not_change')];
					foreach ($list->items as $item)
						{
							if (intval($item['id_ml'])===$menuline_id)
								$selected_item=$item;
							else
								{
									$values[]=$item['position'];
									$labels[]=sm_lang('before').': '.$item['caption_ml'];
								}
						}
					unset($item);
					$values[]=-1;
					$labels[]=sm_lang('last');

					SMAccess::Error404IfEmpty($selected_item);

					sm_title(sm_lang('menu'));
					add_path_modules();
					add_path(sm_lang('module_menu.module_menu_name'), 'index.php?m=menu&d=admin');
					add_path(sm_lang('list_menus'), 'index.php?m=menu&d=listmenu');
					add_path_current();
					$ui=new UI();
					SM::Errors()->DisplayUIErrors($ui);
					$f=new Form('index.php?m=menu&d=posteditline&mid='.$menu_id.'&lid='.$menuline_id.'&sid='.$submenu_from);
					$f->AddText('caption_ml', sm_lang('caption'), true)
						->SetFocus();
					$f->AddText('url', sm_lang('url'));
					$f->AddText('alt_ml', sm_lang('common.alt_text'));
					$f->AddCheckbox('newpage_ml', sm_lang('module_menu.open_in_new_page'));
					$f->AddCheckbox('partial_select', sm_lang('module_menu.select_on_partial_match'));
					$f->AddSelect('p_position', sm_lang('position'), $values, $labels)
						->WithValue(0);
					$f->AddText('attr_ml', sm_lang('module_menu.tag_a_attr'));
					if (SM::Settings('menuitems_use_image')->AsBool())
						$f->AddFile('userfile', sm_lang('common.image'));
					$f->LoadValuesArray($selected_item);
					$f->LoadAllValues(SM::Requests()->POSTAsArray());
					$ui->Add($f);
					$ui->Output(true);
				});

			if (sm_action('listlines'))
				{
					$menu_id = SM::GET('mid')->AsInt();
					$q = new TQuery(sm_table_prefix().'menus');
					$q->AddNumeric('id_menu_m', $menu_id);
					$menuinfo = $q->Get();
					sm_title(sm_lang('menu').': '.$menuinfo['caption_m']);
					add_path_modules();
					add_path(sm_lang('module_menu.module_menu_name'), "index.php?m=menu&d=admin");
					add_path($lang['list_menus'], "index.php?m=menu&d=listmenu");
					add_path($menuinfo['caption_m'], "index.php?m=menu&d=listlines&mid=".$menu_id);
					$m['menu'] = siman_load_menu($menu_id);
					$ui = new UI();
					$t=new Grid('edit');
					$t->AddCol('title', $lang['common']['title'], '40%');
					$t->AddCol('url', $lang['url'], '60%');
					$t->AddCol('open', '', '16', $lang['common']['open']);
					$t->AddEdit();
					$t->AddDelete();
					for ($i = 0; $i < sm_count($m['menu']); $i++)
						{
							$lev = '';
							for ($j = 1; $j < $m['menu'][$i]['level']; $j++)
								{
									$lev .= '-';
								}
							$t->Label('title', $lev.$m['menu'][$i]['caption']);
							if (sm_strlen($m['menu'][$i]['url'])>65)
								{
									$t->Label('url', substr($m['menu'][$i]['url'], 0, 65).'...');
									$t->Expand('url');
									$t->ExpanderHTML($m['menu'][$i]['url']);
								}
							else
								$t->Label('url', $m['menu'][$i]['url']);
							$t->URL('title', $m['menu'][$i]['url'], true);
							$t->Image('open', 'url');
							$t->URL('open', $m['menu'][$i]['url'], true);
							$t->URL('edit', 'index.php?m=menu&d=editline&mid='.$m['menu'][$i]['mid'].'&lid='.$m['menu'][$i]['id'].'&sid='.$m['menu'][$i]['submenu_from']);
							$t->URL('delete', 'index.php?m=menu&d=postdeleteline&mid='.$m['menu'][$i]['mid'].'&lid='.$m['menu'][$i]['id']);
							$t->NewRow();
						}
					if ($t->RowCount()==0)
						$t->SingleLineLabel($lang['messages']['nothing_found']);
					$b=new Buttons();
					$b->AddButton('add', $lang['module_menu']['add_menu_line'], 'index.php?m=menu&d=addline&mid='.$menu_id);
					$ui->AddButtons($b);
					$ui->AddGrid($t);
					$ui->AddButtons($b);
					$ui->Output(true);
				}

			sm_on_action('postedit', function ()
				{
					$menu_id=SM::GET('mid')->AsInt();
					SMAccess::Error404IfEmpty($menu_id);
					$mcaption=SM::POST('caption_m')->EscapedString();
					if (empty($mcaption))
						SM::Errors()->AddError(sm_lang('messages.fill_required_fields'));
					if (SM::Errors()->Count()===0)
						{
							(new TQuery(sm_table_prefix().'menus'))
								->AddString('caption_m', $mcaption)
								->Update('id_menu_m', $menu_id);
							if (SM::Settings('menus_use_image')->AsBool())
								siman_upload_image($menu_id, 'menu');
							sm_notify('messages.add_successful');
							Redirect::Now('index.php?m=menu&d=listmenu');
						}
					else
						sm_set_action('editmenu');
				});

			sm_on_action('editmenu', function ()
				{
					$menu_id=SM::GET('mid')->AsInt();
					SMAccess::Error404IfEmpty($menu_id);
					$menu=(new TQuery(sm_table_prefix().'menus'))
						->AddNumeric('id_menu_m', $menu_id)
						->Get();
					SMAccess::Error404IfEmpty($menu);
					add_path_modules();
					add_path(sm_lang('module_menu.module_menu_name'), 'index.php?m=menu&d=admin');
					add_path_current();
					sm_title(sm_lang('edit_menu'));
					$ui=new UI();
					SM::Errors()->DisplayUIErrors($ui);
					$f=new Form('index.php?m='.sm_current_module().'&d=postedit&mid='.$menu_id);
					$f->AddText('caption_m', sm_lang('caption'))
						->SetFocus();
					if (SM::Settings('menus_use_image')->AsBool())
						$f->AddFile('userfile', sm_lang('common.image'));
					$f->LoadValuesArray($menu);
					$f->LoadValuesArray(SM::Requests()->POSTAsArray());
					$ui->Add($f);
					$ui->Output(true);
				});

			sm_on_action('postdeletemenu', function ()
				{
					$menu_id = SM::GET('mid')->AsInt();
					execsql("DELETE FROM ".sm_table_prefix()."menus WHERE id_menu_m=".$menu_id);
					if (sm_settings('menuitems_use_image') == 1)
						{
							if (file_exists(SM::FilesPath('img/menu'.$menu_id.'.jpg')))
								unlink(SM::FilesPath('img/menu'.$menu_id.'.jpg'));
						}
					$q=new TQuery(sm_table_prefix().'menu_lines');
					$q->AddNumeric('id_menu_ml', $menu_id);
					$q->Remove();
					Redirect::Now('index.php?m=menu&d=listmenu');
				});

			sm_on_action('listmenu', function ()
				{
					sm_title(sm_lang('list_menus'));
					add_path_modules();
					add_path(sm_lang('module_menu.module_menu_name'), 'index.php?m=menu&d=admin');
					add_path_current();
					$ui = new UI();
					$t=new Grid('edit');
					$t->AddCol('title', sm_lang('common.title'), '85%');
					$t->AddCol('add_to_menu', sm_lang('add_to_menu'), '15%');
					$t->AddEdit();
					$t->AddDelete();
					$t->AddCol('stick', '', '16', sm_lang('set_as_block'), '', 'stick.gif');
					$q=new TQuery(sm_table_prefix().'menus');
					$q->OrderBy('caption_m');
					$q->Select();
					foreach ($q->items as $item)
						{
							$t->Label('title', $item['caption_m']);
							$t->Label('add_to_menu', sm_lang('add_to_menu'));
							$t->URL('add_to_menu', sm_tomenuurl($item['caption_m'], sm_fs_url('index.php?m=menu&d=view&mid='.$item['id_menu_m'])));
							$t->URL('title', 'index.php?m=menu&d=listlines&mid='.$item['id_menu_m']);
							$t->URL('edit', 'index.php?m=menu&d=editmenu&mid='.$item['id_menu_m']);
							if ($item['id_menu_m']!=sm_settings('upper_menu_id') && $item['id_menu_m']!=sm_settings('bottom_menu_id') && $item['id_menu_m']!=sm_settings('users_menu_id'))
								$t->URL('delete', 'index.php?m=menu&d=postdeletemenu&mid='.$item['id_menu_m']);
							$t->URL('stick', sm_addblockurl($item['caption_m'], 'menu', $item['id_menu_m'], 'view', 'index.php?m=menu&d=listlines&mid='.$item['id_menu_m']));
							$t->NewRow();
						}
					$b=new Buttons();
					$b->AddButton('add', sm_lang('add_menu'), 'index.php?m=menu&d=add');
					$ui->AddButtons($b);
					$ui->AddGrid($t);
					$ui->AddButtons($b);
					$ui->Output(true);
				});

		}
