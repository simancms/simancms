<?php

	//==============================================================================
	//#revision 2024-02-04
	//==============================================================================

	use SM\SM;
	use SM\UI\Buttons;
	use SM\UI\Form;
	use SM\UI\Grid;
	use SM\UI\UI;
	use SM\User;

	if (!defined("SIMAN_DEFINED"))
		exit('Hacking attempt!');

	if (SM::Settings('allow_private_messages')->AsBool() && SM::isLoggedIn())
		{

			if (!defined("ACCOUNT_MEMBERS_PRIVMSG_FUNCTIONS_DEFINED"))
				{
					function siman_get_userid_by_name($name)
						{
							$sql = "SELECT * FROM ".sm_global_table_prefix()."users WHERE ".database_get_fn_name('lower')."(login)='".dbescape(strtolower($name))."' LIMIT 1";
							$result = execsql($sql);
							$id = 0;
							while ($row = database_fetch_object($result))
								{
									$id = $row->id_user;
								}
							return $id;
						}

					function siman_get_privmsgcount($user)
						{
							$result['inbox']['all'] = 0;
							$result['inbox']['unread'] = 0;
							$result['outbox']['all'] = 0;
							$sql = "SELECT count(*), sum(unread_privmsg), sum(folder_privmsg) FROM ".sm_global_table_prefix()."privmsg WHERE folder_privmsg=1 AND id_sender_privmsg=$user OR folder_privmsg=0 AND id_recipient_privmsg=$user";
							$rezult = execsql($sql);
							while ($row = database_fetch_row($rezult))
								{
									$result['inbox']['all'] = $row[0];
									$result['inbox']['unread'] = $row[1];
									$result['outbox']['all'] = $row[2];
								}
							return $result;
						}

					define("ACCOUNT_MEMBERS_PRIVMSG_FUNCTIONS_DEFINED", 1);
				}

			sm_on_action('viewprivmsg', function ()
				{
					sm_page_viewid('account-viewprivmsg');
					sm_template('account');
					if (SM::GET('folder')->isEmpty())
						SM::GET('folder')->SetValue('inbox');
					$tmp_folder = sm_getvars('folder');
					if (strcmp($tmp_folder, 'outbox') == 0)
						{
							sm_title(sm_lang('module_account.outbox'));
							$tmp_filter = ' folder_privmsg=1 AND id_sender_privmsg='.SM::User()->ID();
						}
					if (empty($tmp_filter))
						{
							sm_title(sm_lang('module_account.inbox'));
							$tmp_filter = ' folder_privmsg=0 AND id_recipient_privmsg='.SM::User()->ID();
							$tmp_folder = 'inbox';
						}
					if (empty($tmp_filter))
						$tmp_filter='1=2';
					$sql = "SELECT * FROM ".sm_global_table_prefix()."privmsg, ".sm_global_table_prefix()."users ";
					if (strcmp($tmp_folder, 'outbox') == 0)
						$sql .= " WHERE ".sm_global_table_prefix()."privmsg.id_sender_privmsg=".sm_global_table_prefix()."users.id_user";
					else
						$sql .= " WHERE ".sm_global_table_prefix()."privmsg.id_recipient_privmsg=".sm_global_table_prefix()."users.id_user";
					$sql .= ' AND '.$tmp_filter;
					$sql .= ' ORDER BY time_privmsg DESC ';
					$ui=new UI();
					$b=new Buttons();
					$b->Button(sm_lang('module_account.send_message'), 'index.php?m=account&d=sendprivmsg');
					$b->Button(sm_lang('module_account.inbox'), 'index.php?m=account&d=viewprivmsg&folder=inbox');
					if (SM::GET('folder')->isStringEqual('inbox'))
						$b->HighlightPrimary();
					$b->Button(sm_lang('module_account.outbox'), 'index.php?m=account&d=viewprivmsg&folder=outbox');
					if (SM::GET('folder')->isStringEqual('outbox'))
						$b->HighlightPrimary();
					$ui->Add($b);
					$t = new Grid('edit');
					$t->AddCol('ico', '', '16');
					$t->AddCol('title', sm_lang('common.title'), '80%');
					$t->AddCol('time', sm_lang('module_account.sent'), '10%');
					$t->AddCol('user', sm_lang('user'), '10%');
					$t->AddDelete();
					$result = execsql($sql);
					while ($row = database_fetch_object($result))
						{
							if ($row->folder_privmsg == 0 && $row->unread_privmsg == 1)
								$t->Image('ico', 'newmessage.gif');
							else
								$t->Image('ico', 'message.gif');
							$t->Label('title', $row->theme_privmsg);
							$t->URL('title', 'index.php?m=account&d=readprivmsg&id='.$row->id_privmsg.'&folder='.$tmp_folder);
							$t->Label('user', $row->login);
							$t->Label('time', date(sm_datetime_mask(), $row->time_privmsg));
							$t->URL('delete', 'index.php?m=account&d=postdeleteprivmsg&id='.$row->id_privmsg.'&folder='.$tmp_folder);
							$t->NewRow();
						}
					if ($t->RowCount()===0)
						$t->SingleLineLabel(sm_lang('messages.nothing_found'));
					$ui->Add($t);
					$ui->Output(true);
				});

			sm_on_action('postsendprivmsg', function ()
				{
					if (SM::POST('p_recipient')->isEmpty())
						{
							SM::Errors()->AddError(sm_lang('module_account.error_message_recipient'));
						}
					elseif (SM::POST('p_theme')->isEmpty() || SM::POST('p_text')->isEmpty())
						{
							SM::Errors()->AddError(sm_lang('messages.fill_required_fields'));
						}
					elseif (siman_get_userid_by_name(dbescape(sm_postvars('p_recipient'))) < 1)
						{
							SM::Errors()->AddError(sm_lang('module_account.error_message_recipient'));
						}
					if (SM::Errors()->Count()>0)
						{
							sm_set_action('sendprivmsg');
						}
					else
						{
							$id_sender_privmsg = SM::User()->ID();
							$id_recipient_privmsg = siman_get_userid_by_name(SM::POST('p_recipient')->EscapedString());
							$folder_privmsg = 0;
							$unread_privmsg = 1;
							$theme_privmsg = dbescape(SM::POST('p_theme')->EscapedString());
							$body_privmsg = dbescape(SM::POST('p_text')->EscapedString());
							$time_privmsg = time();
							$sql = "INSERT INTO ".sm_global_table_prefix()."privmsg (`id_sender_privmsg`, `id_recipient_privmsg`, `folder_privmsg`, `unread_privmsg`, `theme_privmsg`, `body_privmsg`, `time_privmsg`) VALUES('$id_sender_privmsg', '$id_recipient_privmsg', '$folder_privmsg', '$unread_privmsg', '$theme_privmsg', '$body_privmsg', '$time_privmsg')";
							execsql($sql);
							$folder_privmsg = 1;
							$unread_privmsg = 0;
							$sql = "INSERT INTO ".sm_global_table_prefix()."privmsg (`id_sender_privmsg`, `id_recipient_privmsg`, `folder_privmsg`, `unread_privmsg`, `theme_privmsg`, `body_privmsg`, `time_privmsg`) VALUES('$id_sender_privmsg', '$id_recipient_privmsg', '$folder_privmsg', '$unread_privmsg', '$theme_privmsg', '$body_privmsg', '$time_privmsg')";
							execsql($sql);
							log_write(LOG_USEREVENT, sm_lang('module_account.log.user_send_privmsg'));
							sm_redirect('index.php?m=account&d=viewprivmsg&folder=inbox');
						}
				});

			sm_on_action('sendprivmsg', function ()
				{
					sm_title(sm_lang('module_account.send_message'));
					$ui=new UI();
					SM::Errors()->DisplayUIErrors($ui);
					$f=new Form('index.php?m='.sm_current_module().'&d=postsendprivmsg');
					$f->AddText('p_recipient', sm_lang('module_account.recipient'))
						->SetFocus();
					$f->AddText('p_theme', sm_lang('module_account.message_theme'));
					$f->AddTextarea('p_text', sm_lang('module_account.message_text'));
					$f->LoadValuesArray(SM::Requests()->POSTAsArray());
					$ui->Add($f);
					$ui->Output(true);
				});

			if (sm_action('readprivmsg'))
				{
					sm_page_viewid('account-readprivmsg');
					sm_template('account');
					sm_title(sm_lang('module_account.read_message'));
					if (SM::GET('folder')->isEmpty())
						SM::GET('folder')->SetValue('inbox');
					$privmsg=TQuery::ForTable(sm_global_table_prefix().'privmsg')
						->AddWhere('id_privmsg', SM::GET('id')->AsInt())
						->Get();
					if (empty($privmsg['id_privmsg']))
						return;
					if (intval($privmsg['id_sender_privmsg'])!==SM::User()->ID() && intval($privmsg['id_recipient_privmsg'])!==SM::User()->ID())
						return;
					TQuery::ForTable(sm_global_table_prefix().'privmsg')
						  ->AddNumeric('unread_privmsg', 0)
						  ->Update('id_privmsg', SM::GET('id')->AsInt());

					$sender=new User($privmsg['id_sender_privmsg']);
					$recipient=new User($privmsg['id_recipient_privmsg']);
					add_path_home();
					if (intval($privmsg['id_sender_privmsg'])===SM::User()->ID())
						add_path(sm_lang('module_account.outbox'), 'index.php?m=account&d=viewprivmsg&folder=outbox');
					else
						add_path(sm_lang('module_account.inbox'), 'index.php?m=account&d=viewprivmsg&folder=inbox');
					add_path_current();

					$ui=new UI();
					$ui->div(sm_lang('module_account.sender').': '.$sender->Login());
					$ui->div(sm_lang('module_account.recipient').': '.$recipient->Login());
					$ui->div(sm_lang('module_account.message_theme').': '.$privmsg['theme_privmsg']);
					$ui->div(sm_lang('common.time').': '.date(sm_datetime_mask(), $privmsg['time_privmsg']));
					$ui->hr('sm-privmsg-body-separator');
					$ui->div(nl2br($privmsg['body_privmsg']));
					$ui->AddBlock(sm_lang('module_account.reply'));
					$f=new Form('index.php?m='.sm_current_module().'&d=postsendprivmsg');
					$f->AddHidden('p_recipient', $sender->ID()===SM::User()->ID()?$recipient->Login():$sender->Login());
					$f->AddText('p_theme', sm_lang('module_account.message_theme'))
						->WithValue('Re: '.$privmsg['theme_privmsg']);
					$f->AddTextarea('p_text', sm_lang('module_account.message_text'))
						->WithValue("\n\n-------------------\n> ".str_replace("\n", "\n> ", html_entity_decode($privmsg['body_privmsg'])));
					$f->SaveButton(sm_lang('module_account.reply'));
					$ui->Add($f);
					$ui->Output(true);
				}

			sm_on_action('postdeleteprivmsg', function ()
				{
					execsql("DELETE FROM ".sm_global_table_prefix()."privmsg WHERE id_privmsg=".intval(sm_getvars('id'))." and (folder_privmsg=1 and id_sender_privmsg=".SM::User()->ID()." or folder_privmsg=0 and id_recipient_privmsg=".SM::User()->ID().")");
					sm_redirect('index.php?m=account&d=viewprivmsg&folder='.sm_getvars('folder'));
				});

		}
	