<?php

	//------------------------------------------------------------------------------
	//|            Content Management System SiMan CMS                             |
	//|              http://simancms.apserver.org.ua                               |
	//------------------------------------------------------------------------------

	/*
	Module Name: Download
	Module URI: http://simancms.apserver.org.ua/modules/download/
	Description: Downloads management module. Base CMS module
	Version: 1.6.25
	Author URI: http://simancms.apserver.org.ua/
	*/

	use SM\SM;

	sm_default_action('view');

	sm_on_action(['attachment', 'showattachedfile'], function ()
		{
			$att = getsql("SELECT * FROM ".sm_table_prefix()."downloads WHERE userlevel_download<=".SM::User()->Level()." AND id_download=".SM::GET('id')->AsInt().' LIMIT 1');
			$filename=SM::FilesPath('download/attachment'.SM::GET('id')->AsInt());
			if (!empty($att['id_download']) && file_exists($filename))
				{
					sm_session_close();
					header("Content-type: ".$att['attachment_type']);
					if (!sm_action('showattachedfile'))
						header("Content-Disposition: attachment; filename=".basename($att['file_download']));
					$fp = fopen($filename, 'rb');
					fpassthru($fp);
					fclose($fp);
					exit;
				}
		});

	sm_on_action('view', function ()
		{
			sm_page_viewid('download-view');
			sm_template('download');
			sm_title(sm_lang('module_download.downloads'));
			$i = 0;
			if ($result = execsql("SELECT * FROM ".sm_table_prefix()."downloads WHERE attachment_from='-' AND userlevel_download<=".SM::User()->Level()))
				{
					while ($row=database_fetch_assoc($result))
						{
							$m['files'][$i]['id']=$row['id_download'];
							$m['files'][$i]['file']=$row['file_download'];
							$m['files'][$i]['download_url']=SM::FilesURL('download/'.$row['file_download']);
							$m['files'][$i]['description']=$row['description_download'];
							$m['files'][$i]['sizeK']=round(filesize(SM::FilesPath('download/'.$row['file_download']))/1024, 2);
							$m['files'][$i]['sizeM']=round($m['files'][$i]['sizeK']/1024, 2);
							if ($m['files'][$i]['sizeM']>=1)
								$m['files'][$i]['size_label']=$m['files'][$i]['sizeM'].' M';
							else
								$m['files'][$i]['size_label']=$m['files'][$i]['sizeK'].' K';
							sm_add_content_modifier($m['files'][$i]['description']);
							$i++;
						}
				}
		});

	if (SM::isAdministrator() || SM::Settings('perm_downloads_management_level')->AsInt()>0 && SM::Settings('perm_downloads_management_level')->AsInt()<=SM::User()->Level())
		include(SM::ModulesPath('inc/adminpart/download.php'));