<?php

	//------------------------------------------------------------------------------
	//|            Content Management System SiMan CMS                             |
	//|              http://simancms.apserver.org.ua                               |
	//------------------------------------------------------------------------------

	/*
	Module Name: Simple Shortcodes
	Module URI: http://simancms.apserver.org.ua/modules/simpleshortcodes/
	Description: Simple shortcodes to use in your texts
	Version: 2021-12-01
	Author: SiMan CMS Team
	Author URI: http://simancms.apserver.org.ua/
	*/

	use SM\Common\Redirect;
	use SM\Core\SMURL;
	use SM\SM;

	if (!defined("SIMAN_DEFINED"))
		exit('Hacking attempt!');

	if (SM::isAdministrator())
		{
			sm_include_lang('simpleshortcodes');
			if (sm_action('admin'))
				{
					sm_template('simpleshortcodes');
					add_path_modules();
					add_path($lang['module_simpleshortcodes']['module_simpleshortcodes'], 'index.php?m=simpleshortcodes&d=admin');
					sm_title($lang['settings']);
				}
			if (sm_action('install'))
				{
					sm_register_module('simpleshortcodes', $lang['module_simpleshortcodes']['module_simpleshortcodes']);
					sm_register_autoload('simpleshortcodes');
					sm_register_postload('simpleshortcodes');
					Redirect::Now(SMURL::AdminModulesManagement());
				}
			if (sm_action('uninstall'))
				{
					sm_unregister_module('simpleshortcodes');
					sm_unregister_autoload('simpleshortcodes');
					sm_unregister_postload('simpleshortcodes');
					Redirect::Now(SMURL::AdminModulesManagement());
				}
		}

