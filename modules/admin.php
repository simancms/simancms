<?php

	//------------------------------------------------------------------------------
	//|            Content Management System SiMan CMS                             |
	//|              http://simancms.apserver.org.ua                               |
	//------------------------------------------------------------------------------

	//==============================================================================
	//#ver 1.6.24
	//#revision 2023-10-06
	//==============================================================================

	use SM\Common\Helpers\SMStrings;
	use SM\Common\Redirect;
	use SM\Core\SMURL;
	use SM\SM;
	use SM\SMDBData;
	use SM\UI\Buttons;
	use SM\UI\Form;
	use SM\UI\Grid;
	use SM\UI\Navigation;
	use SM\UI\Tabs;
	use SM\UI\UI;
	use SM\UI\FA;

	if (!defined("SIMAN_DEFINED"))
		exit('Hacking attempt!');

	if (!defined("ADMIN_FUNCTIONS_DEFINED"))
		{
			sm_include_lang('admin');
			function sm_get_module_info($filename)
				{
					if (!file_exists($filename))
						return false;
					$fh = fopen($filename, 'r');
					$info = fread($fh, 2048);
					fclose($fh);
					$start = sm_strpos($info, 'Module Name:');
					if ($start !== false && sm_strpos($info, '*/', $start) !== false)
						{
							$info = substr($info, $start, sm_strpos($info, '*/', $start) - $start);
							$items=nllistToArray($info, true);
							for ($i=0; $i<sm_count($items); $i++)
								{
									$item=explode(':', $items[$i]);
									$key=sm_getnicename(trim($item[0]));
									$value='';
									for ($j=1; $j<sm_count($item); $j++)
										$value.=($j>1?':'.$item[$j]:ltrim($item[$j]));
									$result[$key]=$value;
								}
							return $result;
						}
					else
						return false;
				}
			function siman_clean_resource_url($url)
				{
					$url = trim($url);
					if (strcmp($url, '/')==0)
						$url = '';
					if (substr($url, -1) != '/' && sm_strlen($url)>0)
						$url .= '/';
					if (sm_strpos($url, '://')!==false)
						return substr($url, sm_strpos($url, '://')+3);
					return $url;
				}

			sm_add_cssfile('stylesheetsadmin.css');
			define("ADMIN_FUNCTIONS_DEFINED", 1);
		}

	sm_default_action('view');

	if (SM::isAdministrator())
		{
			if (sm_actionpost('postsettings'))
				{
					$settings_mode=SM::GET('viewmode')->AsStringOrDefault('default');
					//------- Common settings ------------------------------------------------------------------------------
					sm_set_settings('resource_title', SM::POST('p_title')->AsString(), $settings_mode);
					sm_set_settings('resource_url', siman_clean_resource_url(SM::POST('p_url')->AsString()), $settings_mode);
					sm_set_settings('resource_url_mobile', siman_clean_resource_url(SM::POST('resource_url_mobile')->AsString()), $settings_mode);
					sm_set_settings('resource_url_tablet', siman_clean_resource_url(SM::POST('resource_url_tablet')->AsString()), $settings_mode);
					sm_set_settings('resource_url_rewrite', SM::POST('resource_url_rewrite')->AsInt(), $settings_mode);
					if ($settings_mode === 'default')
						{
							sm_set_settings('redirect_scheme', SM::POST('redirect_scheme')->AsString(), $settings_mode);
						}
					sm_set_settings('logo_text', SM::POST('p_logo')->AsString(), $settings_mode);
					sm_set_settings('copyright_text', SM::POST('p_copyright')->AsString(), $settings_mode);
					sm_set_settings('meta_keywords', SM::POST('p_keywords')->AsString(), $settings_mode);
					sm_set_settings('meta_description', SM::POST('p_description')->AsString(), $settings_mode);
					sm_set_settings('default_language', SM::POST('p_lang')->AsString(), $settings_mode);
					sm_set_settings('default_theme', SM::POST('p_theme')->AsString(), $settings_mode);
					sm_set_settings('sidepanel_count', (SM::POST('p_sidepanel_count')->AsInt() <= 0) ? 1 : SM::POST('p_sidepanel_count')->AsInt(), $settings_mode);
					sm_set_settings('default_module', SM::POST('p_module')->AsString(), $settings_mode);
					sm_set_settings('cookprefix', SM::POST('p_cook')->AsString(), $settings_mode);
					sm_set_settings('max_upload_filesize', SM::POST('p_maxfsize')->AsInt(), $settings_mode);
					sm_set_settings('admin_items_by_page', SM::POST('p_adminitems_per_page')->AsInt()<=0 ? 10 : SM::POST('p_adminitems_per_page')->AsInt(), $settings_mode);
					sm_set_settings('search_items_by_page', SM::POST('p_searchitems_per_page')->AsInt()<=0 ? 10 : SM::POST('p_searchitems_per_page')->AsInt(), $settings_mode);
					sm_set_settings('ext_editor', SM::POST('p_exteditor')->AsString(), $settings_mode);
					sm_set_settings('noflood_time', SM::POST('p_floodtime')->AsInt()<=0 ? 600 : SM::POST('p_floodtime')->AsInt(), $settings_mode);
					sm_set_settings('blocks_use_image', SM::POST('p_blocks_use_image')->AsInt(), $settings_mode);
					sm_set_settings('rewrite_index_title', SM::POST('p_rewrite_index_title')->AsString(), $settings_mode);
					sm_set_settings('log_type', SM::POST('p_log_type')->AsInt()<=0 ? 0 : SM::POST('p_log_type')->AsInt(), $settings_mode);
					sm_set_settings('log_store_days', SM::POST('p_log_store_days')->AsInt()<=0 ? 0 : SM::POST('p_log_store_days')->AsInt(), $settings_mode);
					sm_set_settings('image_generation_type', (SM::POST('p_image_generation_type')->AsString() == 'static') ? 'static' : 'dynamic', $settings_mode);
					sm_set_settings('title_delimiter', SM::POST('p_title_delimiter')->AsString(), $settings_mode);
					sm_set_settings('meta_resource_title_position', SM::POST('p_meta_resource_title_position')->AsInt(), $settings_mode);
					if ($settings_mode === 'default')
						{
							sm_set_settings('hide_generator_meta', SM::POST('hide_generator_meta')->AsInt());
						}
					//------- Menu settings ------------------------------------------------------------------------------
					sm_set_settings('upper_menu_id', SM::POST('p_uppermenu')->AsString(), $settings_mode);
					sm_set_settings('bottom_menu_id', SM::POST('p_bottommenu')->AsString(), $settings_mode);
					sm_set_settings('users_menu_id', SM::POST('p_usersmenu')->AsString(), $settings_mode);
					sm_set_settings('menus_use_image', SM::POST('p_menus_use_image')->AsInt(), $settings_mode);
					sm_set_settings('menuitems_use_image', SM::POST('p_menuitems_use_image')->AsInt(), $settings_mode);
					//------- Text settings ------------------------------------------------------------------------------
					sm_set_settings('content_use_preview', SM::POST('p_content_use_preview')->AsInt(), $settings_mode);
					sm_set_settings('content_per_page_multiview', SM::POST('content_per_page_multiview')->AsInt()<=0?10:SM::POST('content_per_page_multiview')->AsInt(), $settings_mode);
					sm_set_settings('allow_alike_content', SM::POST('p_allow_alike_content')->AsInt(), $settings_mode);
					sm_set_settings('alike_content_count', SM::POST('alike_content_count')->AsInt()<=0 ? 5 : SM::POST('alike_content_count')->AsInt(), $settings_mode);
					sm_set_settings('content_use_path', SM::POST('p_content_use_path')->AsInt(), $settings_mode);
					sm_set_settings('content_attachments_count', sm_abs(SM::POST('p_content_attachments_count')->AsInt()), $settings_mode);
					sm_set_settings('content_use_image', SM::POST('p_content_use_image')->AsInt(), $settings_mode);
					sm_set_settings('content_image_preview_width', SM::POST('p_content_image_preview_width')->AsString(), $settings_mode);
					sm_set_settings('content_image_preview_height', SM::POST('p_content_image_preview_height')->AsString(), $settings_mode);
					sm_set_settings('content_image_fulltext_width', SM::POST('p_content_image_fulltext_width')->AsString(), $settings_mode);
					sm_set_settings('content_image_fulltext_height', SM::POST('p_content_image_fulltext_height')->AsString(), $settings_mode);
					sm_set_settings('content_editor_level', SM::POST('content_editor_level')->AsInt(), $settings_mode);
					if ($settings_mode === 'default')
						{
							sm_set_settings('autogenerate_content_filesystem', SM::POST('autogenerate_content_filesystem')->AsInt(), 'content');
						}
					//------- News settings ------------------------------------------------------------------------------
					sm_set_settings('news_use_time', SM::POST('p_news_use_time')->AsInt(), $settings_mode);
					sm_set_settings('news_use_image', SM::POST('p_news_use_image')->AsInt(), $settings_mode);
					sm_set_settings('news_image_preview_width', SM::POST('p_news_image_preview_width')->AsString(), $settings_mode);
					sm_set_settings('news_image_preview_height', SM::POST('p_news_image_preview_height')->AsString(), $settings_mode);
					sm_set_settings('news_image_fulltext_width', SM::POST('p_news_image_fulltext_width')->AsString(), $settings_mode);
					sm_set_settings('news_image_fulltext_height', SM::POST('p_news_image_fulltext_height')->AsString(), $settings_mode);
					sm_set_settings('news_by_page', SM::POST('p_news_per_page')->AsInt()<=0?10:SM::POST('p_news_per_page')->AsInt(), $settings_mode);
					sm_set_settings('news_use_preview', SM::POST('p_news_use_preview')->AsInt(), $settings_mode);
					sm_set_settings('news_anounce_cut', SM::POST('p_news_cut')->AsInt()<=0?300:SM::POST('p_news_cut')->AsInt(), $settings_mode);
					sm_set_settings('short_news_count', SM::POST('p_news_short')->AsInt()<=0?3:SM::POST('p_news_short')->AsInt(), $settings_mode);
					sm_set_settings('short_news_cut', SM::POST('p_short_news_cut')->AsInt()<=0?100:SM::POST('p_short_news_cut')->AsInt(), $settings_mode);
					sm_set_settings('allow_alike_news', SM::POST('p_allow_alike_news')->AsInt(), $settings_mode);
					sm_set_settings('alike_news_count', SM::POST('p_alike_news_count')->AsInt(), $settings_mode);
					sm_set_settings('news_attachments_count', sm_abs(SM::POST('p_news_attachments_count')->AsInt()), $settings_mode);
					sm_set_settings('news_full_list_longformat', SM::POST('news_full_list_longformat')->AsInt(), $settings_mode);
					sm_set_settings('news_editor_level', SM::POST('news_editor_level')->AsInt(), $settings_mode);
					if ($settings_mode === 'default')
						{
							sm_set_settings('autogenerate_news_filesystem', SM::POST('autogenerate_news_filesystem')->AsInt(), 'news');
						}
					//------ User settings ----------------------------------------------------------------
					sm_set_settings('allow_register', SM::POST('p_allowregister')->AsInt(), $settings_mode);
					sm_set_settings('allow_forgot_password', SM::POST('p_allowforgotpass')->AsInt(), $settings_mode);
					sm_set_settings('user_activating_by_admin', SM::POST('p_adminactivating')->AsInt(), $settings_mode);
					sm_set_settings('return_after_login', SM::POST('p_return_after_login')->AsInt(), $settings_mode);
					sm_set_settings('allow_private_messages', SM::POST('p_allow_private_messages')->AsInt(), $settings_mode);
					sm_set_settings('use_email_as_login', SM::POST('p_use_email_as_login')->AsInt(), $settings_mode);
					sm_set_settings('signinwithloginandemail', SM::POST('signinwithloginandemail')->AsInt(), $settings_mode);
					sm_set_settings('redirect_after_login_1', SM::POST('p_redirect_after_login_1')->AsString(), $settings_mode);
					sm_set_settings('redirect_after_login_2', SM::POST('p_redirect_after_login_2')->AsString(), $settings_mode);
					sm_set_settings('redirect_after_login_3', SM::POST('p_redirect_after_login_3')->AsString(), $settings_mode);
					sm_set_settings('redirect_after_register', SM::POST('p_redirect_after_register')->AsString(), $settings_mode);
					sm_set_settings('redirect_after_logout', SM::POST('p_redirect_after_logout')->AsString(), $settings_mode);
					sm_set_settings('redirect_on_success_change_usrdata', SM::POST('redirect_on_success_change_usrdata')->AsString(), $settings_mode);
					//------ Security settings ----------------------------------------------------------------
					sm_set_settings('banned_ip', SM::POST('p_banned_ip')->AsString(), $settings_mode);
					//------ Static texts settings ----------------------------------------------------------------
					sm_set_settings('meta_header_text', SM::POST('p_meta_header_text')->AsString(), $settings_mode);
					sm_set_settings('header_static_text', SM::POST('p_htext')->AsString(), $settings_mode);
					sm_set_settings('footer_static_text', SM::POST('p_ftext')->AsString(), $settings_mode);
					//---- Setup mail settings ------------------------------------------------------------
					sm_set_settings('administrators_email', SM::POST('p_admemail')->AsString(), $settings_mode);
					sm_set_settings('email_signature', SM::POST('p_esignature')->AsString(), $settings_mode);
					//-------------------------------------------------------------------------------------

					include(sm_cms_rootdir().'includes/config.php');
					sm_notify(sm_lang('settings_saved_successful'));
					Redirect::Now('index.php?m=admin&d=settings&viewmode='.$settings_mode);
				}
			if (sm_action('view'))
				{
					if (intval(sm_settings('ignore_update'))!=1)
						{
							if (file_exists(sm_cms_rootdir().'includes/update.php'))
								{
									sm_update_settings('install_not_erased', 1);
								}
						}
					sm_event('beforeadmindashboard');
					sm_title(sm_lang('control_panel'));
					$ui = new UI();
					sm_event('admin-dashboard-ui-start', $ui);
					//--------------------------------------------------------------------------------------------------
					$shortcutsdashboard=new Navigation();
					sm_event('admin-dashboard-shortcuts-nav-start', $shortcutsdashboard);
					if ($shortcutsdashboard->Count()>0)
						{
							$ui->AddBlock(sm_lang('module_admin.shortcuts'));
							$ui->AddDashboard($shortcutsdashboard);
						}
					sm_event('admin-dashboard-shortcuts-nav-end', $shortcutsdashboard);
					//--------------------------------------------------------------------------------------------------
					$ui->AddBlock(sm_lang('control_panel'));
					$navigation=new Navigation();
					sm_event('admin-dashboard-control-nav-start', $navigation);
					$navigation->AddItem(sm_lang('modules_mamagement'), SMURL::AdminModulesManagement())->SetFA('th-large');
					$navigation->AddItem(sm_lang('blocks_mamagement'), 'index.php?m=blocks')->SetFA('thumb-tack ');
					$navigation->AddItem(sm_lang('module_admin.virtual_filesystem'), 'index.php?m=admin&d=filesystem')->SetFA('folder');
					$navigation->AddItem(sm_lang('module_admin.images_list'), 'index.php?m=admin&d=listimg')->SetFA('picture-o');
					$navigation->AddItem(sm_lang('module_admin.optimize_database'), 'index.php?m=admin&d=tstatus')->SetFA('database');
					if (intval(sm_settings('log_type'))>0)
						$navigation->AddItem(sm_lang('module_admin.view_log'), 'index.php?m=admin&d=viewlog')->SetFA('history');
					if (is_writeable(sm_cms_rootdir()) && sm_settings('packages_upload_allowed'))
						$navigation->AddItem(sm_lang('module_admin.upload_package'), 'index.php?m=admin&d=package')->SetFA('upload');
					$navigation->AddItem('robots.txt', 'index.php?m=admin&d=robotstxt')->SetFA('file-code-o');
					$navigation->AddItem('ads.txt', 'index.php?m=admin&d=adstxt')->SetFA('file-text-o');
					$navigation->AddItem(sm_lang('settings'), 'index.php?m=admin&d=settings')->SetFA('cog');
					sm_event('admin-dashboard-control-nav-end', $navigation);
					$ui->Add($navigation);
					unset($navigation);
					//--------------------------------------------------------------------------------------------------
					$ui->AddBlock(sm_lang('user_settings'));
					$navigation=new Navigation();
					sm_event('admin-dashboard-users-nav-start', $navigation);
					$navigation->AddItem(sm_lang('register_user'), 'index.php?m=account&d=adminregister')->SetFA('user-plus');
					$navigation->AddItem(sm_lang('user_list'), 'index.php?m=account&d=usrlist')->SetFA('user');
					$navigation->AddItem(sm_lang('module_account.groups_management'), 'index.php?m=account&d=listgroups')->SetFA('users');
					$navigation->AddItem(sm_lang('module_admin.mass_email'), 'index.php?m=admin&d=massemail')->SetFA('envelope');
					sm_event('admin-dashboard-users-nav-end', $navigation);
					$ui->Add($navigation);
					unset($navigation);
					//--------------------------------------------------------------------------------------------------
					sm_event('admin-dashboard-ui-end', $ui);
					$ui->Output(true);
					sm_event('afteradmindashboard');
				}

			sm_on_action('postuplimg', function ()
				{
					global $_uplfilevars;
					$fs = $_uplfilevars['userfile']['tmp_name'];
					if (SM::POST('p_optional')->isEmpty())
						{
							$fd = basename($_uplfilevars['userfile']['name']);
						}
					else
						{
							$fd = SM::POST('p_optional')->AsString();
						}
					$fd = SM::FilesPath('img/'.$fd);
					if (!sm_is_allowed_to_upload($fd))
						{
							SM::Errors()->AddError(sm_lang('error_file_upload_message'));
							sm_set_action('uplimg');
						}
					elseif (!move_uploaded_file($fs, $fd))
						{
							SM::Errors()->AddError(sm_lang('error_file_upload_message'));
							sm_set_action('uplimg');
						}
					else
						{
							sm_event('afteruploadedimagesaveadmin', [$fd]);
							sm_notify(sm_lang('operation_completed'));
							Redirect::Now('index.php?m=admin&d=listimg');
						}
				});

			sm_on_action('uplimg', function ()
				{
					sm_title(sm_lang('upload_image'));
					add_path_control();
					add_path(sm_lang('module_admin.images_list'), 'index.php?m=admin&d=listimg');
					add_path_current();
					$ui = new UI();
					SM::Errors()->DisplayUIErrors($ui);
					$f = new Form('index.php?m=admin&d=postuplimg');
					$f->AddFile('userfile', sm_lang('file_name'));
					$f->AddText('p_optional', sm_lang('optional_file_name'));
					$f->SaveButton(sm_lang('upload'));
					$ui->AddForm($f);
					$ui->NotificationInfo(sm_lang('module_admin.images_media_notification'));
					$ui->Output(true);
				});

			if (sm_action('addmodule'))
				{
					add_path_modules();
					add_path_current();
					sm_title(sm_lang('module_admin.add_module'));
					$ui = new UI();
					$t=new Grid();
					$t->AddCol('title', sm_lang('module'), '20%');
					$t->AddCol('information', sm_lang('common.information'), '25%');
					$t->AddCol('description', sm_lang('common.description'), '50%');
					$t->AddCol('url', '', '16', sm_lang('common.url'));
					$t->SetHeaderImage('url', 'url');
					$t->AddCol('action', sm_lang('action'), '5%');
					$t->SetAsMessageBox('action', sm_lang('common.are_you_sure'));
					$dir = dir(SM::ModulesPath());
					$i = 0;
					while ($entry = $dir->read())
						{
							if (sm_strpos($entry, '.php') > 0)
								{
									if (in_array($entry, Array('admin.php', 'content.php', 'account.php', 'blocks.php', 'refresh.php', 'menu.php', 'news.php', 'download.php', 'search.php', 'media.php')))
										continue;
									if (in_array(substr($entry, 0, -4), nllistToArray(sm_settings('installed_packages'))))
										continue;
									$info = sm_get_module_info('./modules/'.$entry);
									if ($info === false)
										continue;
									if (!empty($info[sm_getnicename('Module Name')]))
										$t->Label('title', $info[sm_getnicename('Module Name')]);
									else
										$t->Label('title', substr($entry, 0, -4));
									$information='';
									if (!empty($info[sm_getnicename('Version')]))
										$information=sm_lang('module_admin.version').': '.$info[sm_getnicename('Version')].'<br />';
									if (!empty($info[sm_getnicename('Author')]))
										$information.=sm_lang('module_admin.author').': '.$info[sm_getnicename('Author')].'<br />';
									$t->Label('information', $information);
									if (!empty($info[sm_getnicename('Description')]))
										$t->Label('description', $info[sm_getnicename('Description')]);
									if (!empty($info[sm_getnicename('Author URI')]))
										$t->DropDownItem('url', sm_lang('module_admin.author'), $info[sm_getnicename('Author URI')]);
									if (!empty($info[sm_getnicename('Module URI')]))
										$t->DropDownItem('url', sm_lang('module'), $info[sm_getnicename('Module URI')]);
									if ($t->DropDownItemsCount('url')>0)
										$t->Image('url', 'url');
									$t->Label('action', sm_lang('common.install'));
									$t->URL('action', 'index.php?m='.substr($entry, 0, -4).'&d=install');
									$t->NewRow();
									$i++;
								}
						}
					$dir->close();
					if ($t->RowCount()==0)
						$t->SingleLineLabel(sm_lang('messages.nothing_found'));
					$ui->AddGrid($t);
					$ui->Output(true);
				}
			if (sm_action('modules'))
				{
					add_path_modules();
					sm_title(sm_lang('modules_mamagement'));
					$ui = new UI();
					$b = new Buttons();
					$b->AddButton('add', sm_lang('module_admin.add_module'), 'index.php?m=admin&d=addmodule');
					$ui->AddButtons($b);
					$t = new Grid();
					$t->AddCol('title', sm_lang('module'));
					$t->AddCol('information', sm_lang('common.information'), '25%');
					$t->AddCol('description', sm_lang('common.description'), '50%');
					$t->AddCol('url', '', '16', sm_lang('common.url'));
					$t->AddEdit();
					$t->AddCol('delete', '', '16');
					$t->SetHeaderImage('delete', 'transparent.gif');
					$result = execsql('SELECT * FROM '.sm_table_prefix().'modules');
					$i = 0;
					while ($row = database_fetch_assoc($result))
						{
							$info = sm_get_module_info('./modules/'.$row['module_name'].'.php');
							if (!empty($info[sm_getnicename('Module Name')]))
								$t->Label('title', $info[sm_getnicename('Module Name')]);
							else
								$t->Label('title', substr($row['module_name'], 0, -4));
							$information='';
							if (!empty($info[sm_getnicename('Version')]))
								$information=sm_lang('module_admin.version').': '.$info[sm_getnicename('Version')].'<br />';
							if (!empty($info[sm_getnicename('Author')]))
								{
									if (!empty($info[sm_getnicename('Author URI')]))
										$information.=sm_lang('module_admin.author').': <a href="'.$info[sm_getnicename('Author URI')].'" target="_blank">'.$info[sm_getnicename('Author')].'</a><br />';
									else
										$information.=sm_lang('module_admin.author').': '.$info[sm_getnicename('Author')].'<br />';
								}
							$t->Label('information', $information);
							if (!empty($info[sm_getnicename('Description')]))
								$t->Label('description', $info[sm_getnicename('Description')]);
							if (!empty($info[sm_getnicename('Author URI')]))
								$t->DropDownItem('url', sm_lang('module_admin.author'), $info[sm_getnicename('Author URI')]);
							if (!empty($info[sm_getnicename('Module URI')]))
								$t->DropDownItem('url', sm_lang('module'), $info[sm_getnicename('Module URI')]);
							if ($t->DropDownItemsCount('url')>0)
								$t->Image('url', 'url');
							if (!empty($row['module_title']))
								$t->Label('title', $row['module_title']);
							$t->Url('title', 'index.php?m='.$row['module_name'].'&d=admin');
							$t->Url('edit', 'index.php?m=admin&d=chgttl&mid='.$row['id_module']);
							if (!in_array($row['module_name'], Array('content', 'news', 'download', 'menu', 'search', 'media')) && sm_is_installed($row['module_name']))
								{
									$t->Image('delete', 'delete.gif');
									$t->Url('delete', 'index.php?m='.$row['module_name'].'&d=uninstall');
									$t->CustomMessageBox('delete', sm_lang('common.are_you_sure'));
								}
							$t->NewRow();
							$i++;
						}
					$ui->AddGrid($t);
					$ui->AddButtons($b);
					$ui->Output(true);
				}

			sm_on_actionpost('postchgttl', function ()
				{
					$q=new TQuery(sm_table_prefix().'modules');
					$q->AddString('module_title', SM::POST('module_title')->AsString());
					$q->Update('id_module', SM::GET('mid')->AsInt());
					Redirect::Now(SMURL::AdminModulesManagement());
				});

			sm_on_action('chgttl', function ()
				{
					add_path_control();
					add_path_current();
					sm_title(sm_lang('change_title'));
					$ui = new UI();
					$f = new Form('index.php?m=admin&d=postchgttl&mid='.SM::GET('mid')->AsInt());
					$f->AddText('module_title', sm_lang('title'))
						->SetFocus();
					$q=new TQuery(sm_table_prefix().'modules');
					$q->AddNumeric('id_module', SM::GET('mid')->AsInt());
					$f->LoadValuesArray($q->Get());
					$ui->AddForm($f);
					$ui->Output(true);
				});

			sm_on_action('copysettings', function ()
				{
					if (SM::GET('destmode')->isEmpty() || SM::GET('name')->isEmpty())
						return;
					$q = new TQuery(sm_table_prefix().'settings');
					$q->Add('name_settings', dbescape(sm_getvars('name')));
					$q->Add('value_settings', dbescape(sm_settings(sm_getvars('name'))));
					$q->Add('mode', dbescape(sm_getvars('destmode')));
					$q->Insert();
					Redirect::Now('index.php?m=admin&d=settings');
				});

			sm_on_action('remsettings', function ()
				{
					if (SM::GET('destmode')->isEmpty() || SM::GET('name')->isEmpty())
						return;
					$q = new TQuery(sm_table_prefix().'settings');
					$q->Add('name_settings', dbescape(sm_getvars('name')));
					$q->Add('mode', dbescape(sm_getvars('destmode')));
					$q->Remove();
					Redirect::Now('index.php?m=admin&d=settings');
				});

			if (sm_action('settings'))
				{
					$fn_init_value=function ($settings_name, $value) use (&$m) {
						$m['edit_settings'][$settings_name] = $value;
						$m['show_settings'][$settings_name] = 1;
					};
					sm_include_lang('adminsettings');
					sm_template('admin');
					sm_title(sm_lang('settings'));
					sm_add_cssfile('css/adminpart/admin-settings.css');
					if (!empty(sm_getvars('viewmode')))
						$m['mode_settings'] = sm_getvars('viewmode');
					else
						$m['mode_settings'] = 'default';
					$m['list_modes'][0]['mode'] = 'mobile';
					$m['list_modes'][0]['shortcut'] = 'M';
					$m['list_modes'][0]['hint'] = sm_lang('common.device').': '.sm_lang('common.mobile_device');
					$m['list_modes'][0]['profile'] = sm_lang('common.mobile_device');
					$m['list_modes'][1]['mode'] = 'tablet';
					$m['list_modes'][1]['shortcut'] = 'T';
					$m['list_modes'][1]['hint'] = sm_lang('common.device').': '.sm_lang('common.tablet_device');
					$m['list_modes'][1]['profile'] = sm_lang('common.tablet_device');
					add_path_control();
					add_path(sm_lang('settings'), 'index.php?m=admin&d=settings');
					if ($m['mode_settings'] == 'default')
						{
							$m['available_modes'] = $m['list_modes'];
							add_path(sm_lang('common.general'), 'index.php?m=admin&d=settings');
						}
					elseif ($m['mode_settings'] == 'mobile')
						{
							$m['available_modes'] = [];
							add_path(sm_lang('common.mobile_device'), 'index.php?m=admin&d=settings&viewmode=mobile');
						}
					elseif ($m['mode_settings'] == 'tablet')
						{
							$m['available_modes'] = [];
							add_path(sm_lang('common.tablet_device'), 'index.php?m=admin&d=settings&viewmode=tablet');
						}
					if ($m['mode_settings'] === 'default')
						{
							$fn_init_value('redirect_scheme', '');
						}
					$sql = "SELECT * FROM ".sm_table_prefix()."settings WHERE mode='".dbescape($m['mode_settings'])."'";
					$result = execsql($sql);
					while ($row = database_fetch_object($result))
						{
							$fn_init_value($row->name_settings, $row->value_settings);
						}
					for ($i = 0; $i < sm_count($m['available_modes']); $i++)
						{
							$sql = "SELECT * FROM ".sm_table_prefix()."settings WHERE mode='".dbescape($m['available_modes'][$i]['mode'])."'";
							$result = execsql($sql);
							while ($row = database_fetch_object($result))
								{
									$m['extmodes'][$m['available_modes'][$i]['mode']]['show_settings'][$row->name_settings] = 1;
								}
						}
					$dir = dir(sm_cms_rootdir().'lang/');
					$i = 0;
					while ($entry = $dir->read())
						{
							if (strcmp($entry, '.') != 0 && strcmp($entry, '..') != 0 && strcmp($entry, 'index.html') != 0 && sm_strpos($entry, '.php'))
								{
									$m['lang'][$i] = substr($entry, 0, sm_strpos($entry, '.'));
									$i++;
								}
						}
					$dir->close();
					$dir = dir(SM::ThemesPath());
					$i = 0;
					while ($entry = $dir->read())
						{
							if (strcmp($entry, '.') != 0 && strcmp($entry, '..') != 0 && strcmp($entry, 'default') != 0 && strcmp($entry, 'index.html') != 0)
								{
									if (!file_exists(SM::FilesPath('themes/'.$entry)))
										continue;
									$m['themes'][$i] = $entry;
									$i++;
								}
						}
					$dir->close();
					$dir = dir(SM::ExternalLibsPublicPath('editors/'));
					$i = 0;
					while ($entry = $dir->read())
						{
							if (strcmp($entry, '.') != 0 && strcmp($entry, '..') != 0 && strcmp($entry, 'index.html') != 0)
								{
									$m['exteditors'][$i] = $entry;
									$i++;
								}
						}
					$dir->close();
					$q=new TQuery(sm_table_prefix().'modules');
					$q->OrderBy('module_title');
					$q->Open();
					while ($q->Fetch())
						$m['modules'][]=Array(
							'title' => $q->row['module_title'].(empty($q->row['module_title'])?$q->row['module_name']:''),
							'name' => $q->row['module_name'],
							'id' => $q->row['id_module']
						);
					unset($q);
					$q=new TQuery(sm_table_prefix()."menus");
					$q->OrderBy('caption_m');
					$q->Open();
					while ($q->Fetch())
						$m['menus'][]=Array(
							'title' => $q->row['caption_m'],
							'id' => $q->row['id_menu_m']
						);
					unset($q);
					if ($m['mode_settings'] == 'default')
						{
							$m['edit_settings']['autogenerate_content_filesystem'] = sm_get_settings('autogenerate_content_filesystem', 'content');
							$m['show_settings']['autogenerate_content_filesystem'] = 1;
							$m['edit_settings']['autogenerate_news_filesystem'] = sm_get_settings('autogenerate_news_filesystem', 'news');
							$m['show_settings']['autogenerate_news_filesystem'] = 1;
						}
				}
			if (sm_action('tstatus'))
				{
					add_path_control();
					add_path_current();
					sm_title(sm_lang('module_admin.optimize_database'));
					$ui = new UI();
					if (SMDBData::CurrentServerType() === 0 || SMDBData::CurrentServerType() === 3)
						{
							$t = new Grid();
							$t->AddCol('table_name', sm_lang('module_admin.table_name'), '25%');
							$t->AddCol('table_rows', sm_lang('module_admin.table_rows'), '25%');
							$t->AddCol('table_size', sm_lang('module_admin.table_size'), '25%');
							$t->AddCol('table_not_optimized', sm_lang('module_admin.table_not_optimized'), '20%');
							$t->AddCol('table_optimize', sm_lang('module_admin.table_optimize'), '5%');
							$sql = "SHOW TABLE STATUS FROM ".SMDBData::CurrentDatabase();
							$result = execsql($sql);
							$i = 0;
							while ($row = database_fetch_object($result))
								{
									$t->Label('table_name', $row->Name);
									$t->Label('table_rows', $row->Rows);
									$t->Label('table_not_optimized', $row->Data_free);
									$t->Label('table_size', $row->Data_length + $row->Index_length);
									$t->Checkbox('table_optimize', 'p_opt_'.$i, $row->Name, $row->Data_free>0);
									$t->NewRow();
									$i++;
								}
							$ui->html('<form action="index.php?m=admin&d=optimize" method="post">');
							$ui->html('<input type="hidden" name="p_table_count" value="'.$i.'" />');
							$ui->AddGrid($t);
							$ui->div('<input type="submit" value="'.sm_lang('module_admin.optimize_tables').'" />', '', '', 'text-align:right;');
						}
					else
						{
							$ui->NotificationWarning(sm_lang('module_admin.message_no_tables_in_DB'));
						}
					$ui->Output(true);
				}
			if (sm_action('optimize'))
				{
					$tc = SM::POST('p_table_count')->AsString();
					if (SMDBData::CurrentServerType() === 0 || SMDBData::CurrentServerType() === 3)
						{
							for ($i = 0; $i < $tc; $i++)
								{
									if (!empty(sm_postvars('p_opt_'.$i)))
										{
											$sql = "OPTIMIZE TABLE `".dbescape(sm_postvars('p_opt_'.$i))."`";
											$result = execsql($sql);
										}
								}
							sm_notify(sm_lang('module_admin.message_optimize_successfull'));
							Redirect::Now('index.php?m=admin&d=tstatus');
						}
				}
			if (sm_action('viewimg'))
				{
					sm_title(sm_lang('common.image'));
					add_path_control();
					add_path(sm_lang('module_admin.images_list'), 'index.php?m=admin&d=listimg');
					add_path_current();
					$ui = new UI();
					$ui->div_open('', 'text-center');
					$ui->img(SM::FilesURL('img/'.sm_getvars('path')), '', '', 'max-width:400px;');
					$b=new Buttons();
					$b->AddMessageBox('del', sm_lang('common.delete'), 'index.php?m=admin&d=postdelimg&imgn='.urlencode(sm_getvars('path')), sm_lang('module_admin.really_want_delete_image').'?');
					$ui->AddButtons($b);
					$ui->div_close();
					$ui->Output(true);
				}
			if (sm_action('listimg'))
				{
					sm_title(sm_lang('module_admin.images_list'));
					add_path_control();
					add_path_current();
					$ui=new UI();
					$ui->div_open('searchimg', '', empty(sm_getvars('filter'))?'display:none;':'');
					$f=new Form('index.php');
					$f->SetMethodGet();
					$f->AddHidden('m', 'admin');
					$f->AddHidden('d', 'listimg');
					$f->AddText('filter', sm_lang('search'));
					$f->SaveButton(sm_lang('search'));
					$f->LoadValuesArray(sm_getvars());
					$ui->AddForm($f);
					$ui->div_close();
					$b=new Buttons();
					$b->Button(sm_lang('upload_image'), 'index.php?m=admin&d=uplimg&returnto='.urlencode(sm_this_url()));
					$b->AddToggle('searchb', sm_lang('search'), Array('searchimg', 'filter'));
					$t=new Grid();
					$t->AddCol('thumb', sm_lang('common.thumbnail'), '10');
					$t->AddCol('title', sm_lang('module_admin.image_file_name'), '85%');
					$t->AddCol('open', sm_lang('common.open'), '5%');
					$t->AddEdit();
					$t->AddDelete();
					$t->SetAsMessageBox('delete', sm_lang('module_admin.really_want_delete_image'));
					$i = 0;
					$j = -1;
					$files = load_file_list(SM::FilesPath('img/'));
					$offset=sm_abs(intval(sm_getvars('from')));
					$limit=intval(sm_settings('admin_items_by_page'));
					while ($j + 1 < count($files))
						{
							$j++;
							$entry=SMStrings::RemoveBeginning(SM::FilesPath('img/'), $files[$j]);
							if (!empty(sm_getvars('filter')))
								if (sm_strpos($entry, sm_getvars('filter')) === false)
									continue;
							if (strcmp($entry, '.') != 0 && strcmp($entry, '..') != 0 && strcmp($entry, 'index.html') != 0 && strcmp($entry, '.htaccess') != 0)
								{
									if ($i>=$offset && $i<$limit+$offset)
										{
											$t->Image('thumb', sm_thumburl($entry, 50, 50));
											$t->Label('title', $entry);
											$t->Label('open', sm_lang('common.open'));
											$t->URL('title', 'index.php?m=admin&d=viewimg&path='.urlencode($entry));
											$t->URL('open', SM::FilesURL('img/'.$entry), true);
											$t->URL('edit', 'index.php?m=admin&d=renimg&imgn='.urlencode($entry));
											$t->URL('delete', 'index.php?m=admin&d=postdelimg&imgn='.urlencode($entry));
											$t->NewRow();
										}
									$i++;
								}
						}
					if ($t->RowCount()==0)
						$t->SingleLineLabel(sm_lang('messages.nothing_found'));
					$ui->Add($b);
					$ui->Add($t);
					$ui->Add($b);
					$ui->AddPagebarParams($i, $limit, $offset);
					$ui->NotificationInfo(sm_lang('module_admin.images_media_notification'));
					$ui->Output(true);
				}
			if (sm_action('postdelimg'))
				{
					sm_title(sm_lang('module_admin.delete_image'));
					$img = sm_getvars("imgn");
					if (!sm_strpos($img, '..') && !sm_strpos($img, '/') && !sm_strpos($img, '\\'))
						unlink(SM::FilesPath('img/'.$img));
					sm_notify(sm_lang('module_admin.message_delete_image_successful'));
					Redirect::Now('index.php?m=admin&d=listimg');
				}
			if (sm_action('postrenimg'))
				{
					$img1 = sm_getvars("on");
					$img2 = sm_getvars("nn");
					if (!(!sm_strpos($img1, '..') && !sm_strpos($img1, '/') && !sm_strpos($img1, '\\') && !sm_strpos($img2, '..') && !sm_strpos($img2, '/') && !sm_strpos($img2, '\\')) || empty($img1) || empty($img2) || !sm_is_allowed_to_upload($img2))
						{
							$m["error_message"] = sm_lang('module_admin.message_wrong_file_name');
						}
					else
						{
							if (!rename(SM::FilesPath('img/'.$img1), SM::FilesPath('img/'.$img2)))
								$m["error_message"] = sm_lang('module_admin.message_cant_reaname');
						}
					if (empty($m["error_message"]))
						{
							sm_notify(sm_lang('module_admin.message_rename_image_successful'));
							Redirect::Now('index.php?m=admin&d=listimg');
						}
					else
						{
							$m['mode'] = 'renimg';
							SM::GET('imgn')->SetValue($img1);
						}
				}
			if (sm_action('renimg') && !empty(sm_getvars("imgn")))
				{
					sm_title(sm_lang('module_admin.rename_image'));
					add_path_control();
					add_path(sm_lang('module_admin.images_list'), "index.php?m=admin&d=listimg");
					$ui = new UI();
					if (!empty($m['error_message']))
						$ui->div($m['error_message'], '', 'errormessage');
					$f = new Form('index.php', '', 'get');
					$f->AddText('nn', sm_lang('file_name'))
						->SetFocus();
					$f->LoadValuesArray(sm_getvars());
					if (empty(sm_getvars('nn')))
						$f->SetValue('nn', sm_getvars("imgn"));
					$f->AddHidden('m', 'admin');
					$f->AddHidden('d', 'postrenimg');
					$f->AddHidden('on', sm_getvars("imgn"));
					$f->SaveButton(sm_lang('rename'));
					$ui->AddForm($f);
					$ui->Output(true);
				}
			if (sm_action('postmassemail'))
				{
					if (SM::POST('subject')->isEmpty() || SM::POST('message')->isEmpty())
						{
							$error=sm_lang('message_set_all_fields');
							sm_set_action('massemail');
						}
					else
						{
							$result = execsql("SELECT * FROM ".sm_global_table_prefix()."users WHERE get_mail=1");
							while ($row = database_fetch_assoc($result))
								{
									send_mail(sm_website_title()." <".sm_settings('administrators_email').">", $row['email'], SM::POST('subject')->AsString(), SM::POST('message')->AsString());
								}
							sm_notify(sm_lang('operation_completed'));
							Redirect::Now(SMURL::AdminControlPanel());
						}
				}
			if (sm_action('massemail'))
				{
					add_path_control();
					add_path_current();
					sm_title(sm_lang('module_admin.mass_email'));
					$ui = new UI();
					if (!empty($error))
						$ui->NotificationError($error);
					$f = new Form('index.php?m=admin&d=postmassemail');
					$f->AddText('subject', sm_lang('module_admin.mass_email_theme'))
						->SetFocus();
					$f->AddEditor('message', sm_lang('module_admin.mass_email_message'));
					$f->LoadValuesArray(sm_postvars());
					if (count(sm_postvars())==0)
						$f->SetValue('message', sm_settings('email_signature'));
					$ui->AddForm($f);
					$ui->Output(true);
				}
			if (sm_action('filesystem'))
				{
					add_path_control();
					add_path(sm_lang('module_admin.virtual_filesystem'), 'index.php?m=admin&d=filesystem');
					sm_title(sm_lang('module_admin.virtual_filesystem'));
					$offset=sm_abs(sm_getvars('from'));
					$limit=intval(sm_settings('admin_items_by_page'));
					$ui = new UI();
					$t=new Grid();
					$t->AddCol('ico', '', '16');
					$t->AddCol('url', sm_lang('url'), '50%');
					$t->HeaderDropDownItem('url', sm_lang('common.sortingtypes.asc'), sm_this_url(Array('orderby'=>'urlasc', 'from'=>'')));
					$t->HeaderDropDownItem('url', sm_lang('common.sortingtypes.desc'), sm_this_url(Array('orderby'=>'urldesc', 'from'=>'')));
					$t->AddCol('title', sm_lang('common.title'), '50%');
					$t->HeaderDropDownItem('title', sm_lang('common.sortingtypes.asc'), sm_this_url(Array('orderby'=>'titleasc', 'from'=>'')));
					$t->HeaderDropDownItem('title', sm_lang('common.sortingtypes.desc'), sm_this_url(Array('orderby'=>'titledesc', 'from'=>'')));
					$t->AddEdit();
					$t->AddDelete();
					$t->AddMenuInsert();
					$q=new TQuery(sm_table_prefix()."filesystem");
					if (sm_getvars('orderby')=='urldesc')
						$q->OrderBy('filename_fs DESC');
					elseif (sm_getvars('orderby')=='titleasc')
						$q->OrderBy('comment_fs');
					elseif (sm_getvars('orderby')=='titledesc')
						$q->OrderBy('comment_fs DESC');        
					else
						$q->OrderBy('filename_fs');
					$q->Limit($limit);
					$q->Offset($offset);
					$q->Select();
					for ($i = 0; $i<$q->Count(); $i++)
						{
							if (substr($q->items[$i]['filename_fs'], -1) == '/')
								$t->Label('ico', FA::EmbedCodeFor('folder'));
							else
								$t->Label('ico', FA::EmbedCodeFor('file-o'));
							$t->Hint('ico', $q->items[$i]['id_fs']);
							$t->Label('url', $q->items[$i]['filename_fs']);
							$t->Label('title', empty($q->items[$i]['comment_fs'])?'-----':$q->items[$i]['comment_fs']);
							$t->URL('url', $q->items[$i]['filename_fs'], true);
							$t->URL('title', $q->items[$i]['url_fs'], true);
							$t->URL('edit', 'index.php?m=admin&d=editfilesystem&id='.$q->items[$i]['id_fs'].'&returnto='.urlencode(sm_this_url()));
							$t->URL('delete', 'index.php?m=admin&d=postdeletefilesystem&id='.$q->items[$i]['id_fs'].'&returnto='.urlencode(sm_this_url()));
							$t->Menu($q->items[$i]['comment_fs'], $q->items[$i]['filename_fs']);
							$t->NewRow();
						}
					$b=new Buttons();
					$b->AddButton('add', sm_lang('common.add'), 'index.php?m=admin&d=addfilesystem');
					$ui->AddButtons($b);
					$ui->AddGrid($t);
					$ui->AddButtons($b);
					$ui->AddPagebarParams($q->TotalCount(), $limit, $offset);
					$ui->Output(true);
				}
			if (sm_action('postdeletefilesystem'))
				{
					sm_title(sm_lang('common.delete'));
					$sql = "DELETE FROM ".sm_table_prefix()."filesystem WHERE id_fs=".intval(sm_getvars("id"));
					execsql($sql);
					if (!empty(sm_getvars('returnto')))
						Redirect::Now(sm_getvars('returnto'));
					else
						Redirect::Now('index.php?m=admin&d=filesystem');
				}
			if (sm_action('postaddfilesystem', 'posteditfilesystem'))
				{
					$q=new TQuery(sm_table_prefix().'filesystem');
					$q->Add('filename_fs', dbescape(SM::POST('filename_fs')->AsString()));
					$q->Add('url_fs', dbescape(SM::POST('url_fs')->AsString()));
					$q->Add('comment_fs', dbescape(SM::POST('comment_fs')->AsString()));
					if (sm_action('postaddfilesystem'))
						$q->Insert();
					else
						$q->Update('id_fs', intval(sm_getvars('id')));
					if (!empty(sm_getvars('returnto')))
						Redirect::Now(sm_getvars('returnto'));
					else
						Redirect::Now('index.php?m=admin&d=filesystem');
				}
			if (sm_action('addfilesystem', 'editfilesystem'))
				{
					add_path_control();
					add_path(sm_lang('module_admin.virtual_filesystem'), 'index.php?m=admin&d=filesystem');
					$ui = new UI();
					if (!empty($error))
						$ui->div($error, '', 'error alert-error');
					if (sm_action('editfilesystem'))
						{
							sm_title(sm_lang('common.edit'));
							$f=new Form('index.php?m=admin&d=posteditfilesystem&id='.intval(sm_getvars('id')).'&returnto='.urlencode(sm_getvars('returnto')));
						}
					else
						{
							sm_title(sm_lang('common.add'));
							$f=new Form('index.php?m=admin&d=postaddfilesystem&returnto='.urlencode(sm_getvars('returnto')));
						}
					$f->AddText('filename_fs', sm_lang('common.url'))
						->SetFocus();
					$f->AddText('url_fs', sm_lang('module_admin.true_url'));
					$f->AddText('comment_fs', sm_lang('common.comment'));
					if (sm_action('editfilesystem'))
						{
							$q=new TQuery(sm_table_prefix().'filesystem');
							$q->Add('id_fs', intval(sm_getvars('id')));
							$f->LoadValuesArray($q->Get());
							unset($q);
						}
					$f->LoadValuesArray(sm_postvars());
					$ui->AddForm($f);
					$ui->Output(true);
				}

			sm_on_action('viewlog', function ()
				{
					add_path_control();
					add_path(sm_lang('module_admin.view_log'), 'index.php?m=admin&d=viewlog');
					if (intval(sm_settings('log_store_days'))>0)
						{
							$q = new TQuery(sm_table_prefix().'log');
							$q->Add('object_name', 'system');
							$q->Add('time<'.(time()-intval(sm_settings('log_store_days'))*3600*24));
							$q->Remove();
						}
					sm_title(sm_lang('module_admin.view_log'));
					$limit=100;
					$offset=SM::GET('from')->AsAbsInt();
					$ui = new UI();
					$t=new Grid();
					$t->AddCol('time', sm_lang('common.time'), '20%');
					$t->AddCol('description', sm_lang('common.description'), '60%');
					$t->AddCol('ip', 'IP', '10%');
					$t->AddCol('user', sm_lang('user'), '10%');
					$q=new TQuery(sm_table_prefix().'log');
					$q->AddString('object_name', 'system');
					$q->OrderBy('id_log DESC');
					$q->Limit($limit);
					$q->Offset($offset);
					$q->Select();
					for ($i = 0; $i<count($q->items); $i++)
						{
							$t->Label('time', date(sm_datetime_mask(), $q->items[$i]['time']));
							$t->Label('description', htmlescape($q->items[$i]['description']));
							$t->Label('ip', @inet_ntop($q->items[$i]['ip']));
							$t->Label('user', $q->items[$i]['user']);
							$t->NewRow();
						}
					if ($t->RowCount()===0)
						$t->SingleLineLabel(sm_lang('messages.nothing_found'));
					$ui->AddGrid($t);
					$ui->AddPagebarParams($q->TotalCount(), $limit, $offset);
					$ui->Output(true);
				});

			if (sm_action('postpackage') && sm_settings('packages_upload_allowed'))
				{
					if (empty(sm_getvars('typeupload')))
						{
							$fs = $_uplfilevars['userfile']['tmp_name'];
							$fd = basename($_uplfilevars['userfile']['name']);
							$fd = './'.$fd;
							$m['fs'] = $fs;
							$m['fd'] = $fd;
							if (!move_uploaded_file($fs, $fd))
								{
									$m['error_message'] = sm_lang('error_file_upload_message');
									sm_set_action('package');
								}
						}
					elseif (function_exists('curl_init'))
						{
							$ch = curl_init(SM::POST('urlupload')->AsString());
							if (file_exists(sm_cms_rootdir().'urlupload.zip'))
								unlink(sm_cms_rootdir().'urlupload.zip');
							$fp = fopen(sm_cms_rootdir()."urlupload.zip", "w");
							curl_setopt($ch, CURLOPT_FILE, $fp);
							curl_setopt($ch, CURLOPT_HEADER, 0);
							curl_setopt($ch, CURLOPT_FAILONERROR, 1);
							curl_exec($ch);
							$tmperr = curl_error($ch);
							curl_close($ch);
							fclose($fp);
							if (!empty($tmperr))
								{
									$m['error_message'] = $tmperr;
									sm_set_action('package');
									unlink(sm_cms_rootdir().'urlupload.zip');
								}
							else
								$fd = sm_cms_rootdir().'urlupload.zip';
						}
					if (sm_action('postpackage'))
						{
							global $refresh_url;
							require_once(SM::ExternalLibsPublicPath('package/unarchiver.php'));
							$zip = new PclZip($fd);
							$ext = $zip->extract(PCLZIP_OPT_SET_CHMOD, 0777);
							unlink($fd);
							if (intval(sm_settings('ignore_update'))!=1)
								{
									if (file_exists(sm_cms_rootdir().'includes/update.php'))
										{
											include(sm_cms_rootdir().'includes/update.php');
											@unlink(sm_cms_rootdir().'includes/update.php');
											if (file_exists(sm_cms_rootdir().'includes/update.php') && empty($refresh_url))
												sm_update_settings('install_not_erased', 1);
										}
								}
							if (empty($refresh_url))
								Redirect::Now(SMURL::AdminControlPanel());
						}
				}
			if (sm_action('package') && sm_settings('packages_upload_allowed'))
				{
					sm_title(sm_lang('module_admin.upload_package'));
					add_path_control();
					add_path_current();
					$ui = new UI();
					if (!empty($m['error_message']))
						{
							$ui->AddBlock(sm_lang('error'));
							$ui->NotificationError($m['error_message']);
						}
					$tabs=new Tabs();
					$tabs->AddBlock(sm_lang('module_admin.upload_package').' ('.sm_lang('common.file').')');
					$f = new Form('index.php?m=admin&d=postpackage');
					$f->AddFile('userfile', sm_lang('file_name'));
					$f->SaveButton(sm_lang('upload'));
					$tabs->AddForm($f);
					if (function_exists('curl_init'))
						{
							$tabs->AddBlock(sm_lang('module_admin.upload_package').' ('.sm_lang('common.url').')');
							$f = new Form('index.php?m=admin&d=postpackage&typeupload=url');
							$f->AddText('urlupload', sm_lang('common.url'));
							if (sm_getvars('typeupload')=='url')
								{
									$tabs->SetActiveIndex(1);
									$f->SetFocus();
								}
							$f->SaveButton(sm_lang('upload'));
							$tabs->AddForm($f);
						}
					$ui->Add($tabs);
					$ui->Output(true);
				}

			sm_on_action('saverobotstxt', function ()
				{
					sm_update_settings('robots_txt', SM::POST('robotstxtcontent')->AsString(), 'seo');
					Redirect::Now(SMURL::AdminControlPanel());
				});

			sm_on_action('robotstxt', function ()
				{
					add_path_control();
					add_path_current();
					sm_title('robots.txt');
					$ui = new UI();
					$f = new Form('index.php?m=admin&d=saverobotstxt');
					$f->AddTextarea('robotstxtcontent')
						->WithValue(sm_get_settings('robots_txt', 'seo'))
						->SetFocus()
						->MergeColumns();
					$ui->AddForm($f);
					$ui->Output(true);
				});

			sm_on_action('saveadstxt', function ()
				{
					sm_update_settings('ads_txt', SM::POST('robotstxtcontent')->AsString(), 'seo');
					Redirect::Now(SMURL::AdminControlPanel());
				});

			sm_on_action('adstxt', function ()
				{
					if (!sm_settings_exists_in_db('ads_txt', 'seo'))
						{
							if (file_exists(sm_cms_rootdir().'ads.txt'))
								sm_add_settings('ads_txt', file_get_contents(sm_cms_rootdir().'ads.txt'), 'seo');
							else
								sm_add_settings('ads_txt', '', 'seo');
						}
					add_path_control();
					add_path_current();
					sm_title('ads.txt');
					$ui = new UI();
					$f = new Form('index.php?m=admin&d=saveadstxt');
					$f->AddTextarea('robotstxtcontent')
						->WithValue(sm_get_settings('ads_txt', 'seo'))
						->SetFocus()
						->MergeColumns();
					$ui->AddForm($f);
					$ui->Output(true);
				});

		}
