<?php

	$lang['module_galleries']['media_files']='Медіа';
	$lang['module_galleries']['galleries']='Галереї';
	$lang['module_galleries']['gallery']='Галерея';
	$lang['module_galleries']['gallery_thumb']='Мініатюра галереї';
	$lang['module_galleries']['gallery_thumb_width']='Ширина мініатюри галереї';
	$lang['module_galleries']['gallery_thumb_height']='Висота мініатюри галереї';
	$lang['module_galleries']['gallery_default_view']='Вигляд галерeї по замовчуванню';
	$lang['module_galleries']['gallery_view_items_per_row']='Кількість елементів в рядку при перегляді галереї';
	$lang['module_galleries']['galleries_view_items_per_row']='Кількість елементів в рядку при перегляді галерей';
	$lang['module_galleries']['galleries_sort']='Сортування галерей';
	$lang['module_galleries']['media_thumb_width']='Ширина мініатюри зображення';
	$lang['module_galleries']['media_thumb_height']='Висота мініатюри зображення';
	$lang['module_galleries']['media_medium_width']='Ширина зображення';
	$lang['module_galleries']['media_meduim_height']='Висота зображення';
	$lang['module_galleries']['media_allowed_extensions']='Дозволені розширення медіа-файлів';
	$lang['module_galleries']['media_edit_after_upload']='Редагувати після завантаження';
	$lang['module_galleries']['all_images_in_one_page']='Всі зображення на одній сторінці';
	$lang['module_galleries']['media_erase_original_image']='Видалити оригінальне зображення після завантаження';
	$lang['module_galleries']['add_at_least_one_image']='Додайте хоча б одне зображення';
	$lang['module_galleries']['custom_no_image']='Власне зображення NO IMAGE';
	$lang['module_galleries']['custom_gallery_image_size_zero_default']='Розміри зображення в галереї (0 для стандартних розмірів)';

