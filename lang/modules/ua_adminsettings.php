<?php

$lang['module_adminsettings']['module_adminsettings']='Розширене налаштування';
$lang['module_adminsettings']['really_want_delete']='Ви дійсно хочете видалити параметр налаштування? Це може спричинити непоправні наслідки!';
$lang['module_adminsettings']['redirect_scheme']='Автоматично перенаправляти схему URL';
$lang['module_adminsettings']['no_redirect']='Відсутнє перенаправлення';

