<?php

	$lang['module_galleries']['media_files']='Media';
	$lang['module_galleries']['galleries']='Galleries';
	$lang['module_galleries']['gallery']='Gallery';
	$lang['module_galleries']['gallery_thumb']='Gallery thumb';
	$lang['module_galleries']['gallery_thumb_width']='Gallery thumbnail width';
	$lang['module_galleries']['gallery_thumb_height']='Gallery thumbnail height';
	$lang['module_galleries']['gallery_default_view']='Gallery default view';
	$lang['module_galleries']['gallery_view_items_per_row']='Gallery view items per row';
	$lang['module_galleries']['galleries_view_items_per_row']='Galleries view items per row';
	$lang['module_galleries']['galleries_sort']='Galleries sorting order';
	$lang['module_galleries']['media_thumb_width']='Image thumbnail width';
	$lang['module_galleries']['media_thumb_height']='Image thumbnail height';
	$lang['module_galleries']['media_medium_width']='Image width';
	$lang['module_galleries']['media_meduim_height']='Image height';
	$lang['module_galleries']['media_allowed_extensions']='Allowed media files extensions';
	$lang['module_galleries']['media_edit_after_upload']='Edit after upload';
	$lang['module_galleries']['all_images_in_one_page']='All images at single page';
	$lang['module_galleries']['media_erase_original_image']='Erase original image after upload';
	$lang['module_galleries']['add_at_least_one_image']='Please add at least one image';
	$lang['module_galleries']['custom_no_image']='Custom no image URL';
	$lang['module_galleries']['custom_gallery_image_size_zero_default']='Custom image size for this gallery (leave 0 for default size)';
