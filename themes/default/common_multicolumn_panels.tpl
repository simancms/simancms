{strip}
	{$data.global_container.start_html}
	{foreach from=$data.panels item=panel}
		{$panel.container_start_html}
			{include file="common_adminpanel.tpl" panelblocks=$panel.panel}
		{$panel.container_end_html}
	{/foreach}
	{$data.global_container.end_html}
{/strip}