<?php

	use SM\Themes\CurrentTheme;

	CurrentTheme::SetConfigVar('ui.nav.globalclass', 'nav nav-pills nav-stacked');

	CurrentTheme::SetConfigVar('ui.form.nohighlight', true);
	CurrentTheme::SetConfigVar('ui.form.globalclass', '');
	CurrentTheme::SetConfigVar('ui.form.rowclass', '');
	CurrentTheme::SetConfigVar('ui.form.textclass', '');
	CurrentTheme::SetConfigVar('ui.form.selectclass', '');
	CurrentTheme::SetConfigVar('ui.form.textareaclass', '');
	CurrentTheme::SetConfigVar('ui.form.radiogroup_class', '');

	CurrentTheme::SetConfigVar('ui.grid.globalclass', '');
	CurrentTheme::SetConfigVar('ui.grid.header_tag', '');
	CurrentTheme::SetConfigVar('ui.grid.header_th_wrapper_begin', '');
	CurrentTheme::SetConfigVar('ui.grid.header_th_wrapper_end', '');

	CurrentTheme::SetConfigVar('content_editor.controlbuttonsclass', 'btn-xs');

	CurrentTheme::SetConfigVar('ui.buttons.htmlbegin', '');
	CurrentTheme::SetConfigVar('ui.buttons.htmlend', '');
	CurrentTheme::SetConfigVar('ui.buttons.globalclass', '');
	CurrentTheme::SetConfigVar('ui.buttons.buttonseparator', '');
	CurrentTheme::SetConfigVar('ui.buttons.buttonclass', '');
	CurrentTheme::SetConfigVar('ui.buttons.buttonbegin', '');
	CurrentTheme::SetConfigVar('ui.buttons.buttonend', '');
	CurrentTheme::SetConfigVar('ui.buttons.dropdown_a_class', '');
