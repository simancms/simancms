{include file="page_header.tpl"}


<nav class="navbar navbar-expand-lg navbar-dark bg-dark" aria-label="Eighth navbar example">
	<div class="container">
		<a class="navbar-brand" href="{$sm.s.home_url}" title="{$_settings.logo_text}">{$_settings.resource_title}</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#full_navigation" aria-controls="full_navigation" aria-expanded="false" aria-label="{$lang.p_menu.main_menu}">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="full_navigation">
			{if $_settings.upper_menu_id neq ""}
				{include file="uppermenu.tpl"}
			{/if}
			<form action="index.php" role="search">
				<input type="hidden" name="m" value="{if $_settings.search_module neq ""}{$_settings.search_module}{else}search{/if}">
				<input type="hidden" name="d" value="{if $_settings.search_action neq ""}{$_settings.search_action}{else}{/if}">
				<input class="form-control" type="text" placeholder="{$lang.search}" aria-label="{$lang.search}" name="{if $_settings.search_query_var neq ""}{$_settings.search_query_var}{else}q{/if}">
			</form>
		</div>
	</div>
</nav>

<main class="container">
	<div class="row">
		<div class="col-md-9">
			{include file="path.tpl"}

			{$special.document.panel[0].beforepanel}
			{assign var=loop_center_panel value=1}
			{assign var=show_center_panel value=1}
			{section name=mod_index loop=$modules step=1 start=1}
				{if $_settings.main_block_position lt $loop_center_panel and $show_center_panel eq 1}
					{assign var=show_center_panel value=0}
					{assign var=index value=0}
					{assign var=mod_name value=$modules[0].module}
					{$special.document.block[0].beforeblock}
					{include file="$mod_name.tpl" m=$modules[0]}
					{$special.document.block[0].afterblock}
				{/if}
				{if $modules[mod_index].panel eq "center"}
					{assign var=index value=$smarty.section.mod_index.index}
					{assign var=mod_name value=$modules[mod_index].module}
					{$special.document.block[mod_index].beforeblock}
					{include file="$mod_name.tpl" m=$modules[mod_index]}
					{$special.document.block[mod_index].afterblock}
					{assign var=loop_center_panel value=$loop_center_panel+1}
				{/if}
			{/section}
			{if $show_center_panel eq 1}
				{assign var=show_center_panel value=0}
				{assign var=index value=0}
				{assign var=mod_name value=$modules[0].module}
				{$special.document.block[0].beforeblock}
				{include file="$mod_name.tpl" m=$modules[0]}
				{$special.document.block[0].afterblock}
			{/if}
			{$special.document.panel[0].afterpanel}
		</div>
		<div class="col-md-3">
			{$special.document.panel[1].beforepanel}
			{section name=mod_index loop=$modules step=1}
				{if $modules[mod_index].panel eq "1"}
					{assign var=index value=$smarty.section.mod_index.index}
					{assign var=mod_name value=$modules[mod_index].module}
					{$special.document.block[mod_index].beforeblock}
					{include file="$mod_name.tpl" m=$modules[mod_index]}
					{$special.document.block[mod_index].afterblock}
				{/if}
			{/section}
			{$special.document.panel[1].afterpanel}
		</div>
	</div>
	<div class="row">
		{$special.document.panel[2].beforepanel}
		{section name=mod_index loop=$modules step=1}
			{if $modules[mod_index].panel eq "2"}
				{assign var=index value=$smarty.section.mod_index.index}
				{assign var=mod_name value=$modules[mod_index].module}
				{$special.document.block[mod_index].beforeblock}
				{include file="$mod_name.tpl" m=$modules[mod_index]}
				{$special.document.block[mod_index].afterblock}
			{/if}
		{/section}
		{$special.document.panel[2].afterpanel}
	</div>
</main>

<div class="container">
	<footer class="py-3 my-4">
		{if $_settings.bottom_menu_id neq ""}{include file="bottommenu.tpl"}{/if}
		<p class="text-center text-muted">{$_settings.copyright_text}</p>
		<p class="text-center text-muted">Powered by <a href="http://simancms.apserver.org.ua/" target="_blank">SiMan CMS</a></p>
	</footer>
</div>


{include file="page_footer.tpl"}