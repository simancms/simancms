{if $modules[$index].pages.pages gt "1"}
	<nav>
		<ul class="pagination">
			{section name="i" loop=5000 max=$modules[$index].pages.pages start="1"}
				<li class="page-item {if $smarty.section.i.index eq $modules[$index].pages.selected} active{/if}">
					<a href="{$modules[$index].pages.url}&from={math equation="x*y" x=$modules[$index].pages.interval y=$smarty.section.i.index-1}" class="page-link">{$smarty.section.i.index}</a>
				</li>
			{/section}
		</ul>
	</nav>
{/if}