<ul class="nav justify-content-center border-bottom pb-3 mb-3">
    {section name=i loop=$special.bottommenu}
		<li class="nav-item">{$special.bottommenu[i].html_begin}<a href="{$special.bottommenu[i].url}" class="nav-link px-2 text-muted {if $special.bottommenu[i].active eq "1"}bottomMenuSelected{else}bottomMenuLine{/if}"{if $special.bottommenu[i].alt neq ""} alt="{$special.bottommenu[i].alt}"{/if}{if $special.bottommenu[i].newpage eq "1"} target="_blank"{/if}{$special.bottommenu[i].attr}>{$special.bottommenu[i].caption}</a>{$special.bottommenu[i].html_end}</li>
    {/section}
</ul>