<ul class="navbar-nav me-auto mb-2 mb-lg-0">
	{section name=i loop=$special.uppermenu}
		{if $special.uppermenu[i].level eq 1}
			{$special.uppermenu[i].html_begin}
			<li class="nav-item {if $special.uppermenu[i].is_submenu eq 1} dropdown{/if}" id="uppermenu{$smarty.section.i.index}">
				<a class="nav-link {if $special.uppermenu[i].is_submenu eq 1}dropdown-toggle{/if}{if $special.uppermenu[i].active eq "1"} active{/if}"
				   {if $special.uppermenu[i].is_submenu eq 1}aria-expanded="false" data-bs-toggle="dropdown"
				   id="uppermenu{$smarty.section.i.index}-link" href="javascript:;"{else}
				   href="{$special.uppermenu[i].url}"{/if}{if $special.uppermenu[i].alt neq ""} alt="{$special.uppermenu[i].alt}"{/if}{if $special.uppermenu[i].newpage eq "1"} target="_blank"{/if}{$special.uppermenu[i].attr}>
					{$special.uppermenu[i].caption}
				</a>
				{if $special.uppermenu[i].is_submenu eq 1}
					<ul class="dropdown-menu" aria-labelledby="uppermenu{$smarty.section.i.index}-link">
						{section name=j loop=$special.uppermenu start=i}
							{if $special.uppermenu[j].level gt "1" and $special.uppermenu[j].submenu_from eq $special.uppermenu[i].id}
								{$special.uppermenu[j].html_begin}
								<li><a href="{$special.uppermenu[j].url}"
									   class="{if $special.uppermenu[j].active eq "1"}upperMenuSelected{else}upperMenuLine{/if} dropdown-item"{if $special.uppermenu[j].alt neq ""} alt="{$special.uppermenu[j].alt}"{/if}{if $special.uppermenu[j].newpage eq "1"} target="_blank"{/if}{$special.uppermenu[j].attr}>{$special.uppermenu[j].caption}</a>
								</li>{$special.uppermenu[j].html_end}
							{/if}
						{/section}
					</ul>
				{/if}
			</li>{$special.uppermenu[i].html_end}
		{/if}
	{/section}
</ul>
