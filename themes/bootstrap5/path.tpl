{if $special.pathcount gt 0}
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      {section name=path_index loop=$special.path}
        <li class="breadcrumb-item"><a href="{$special.path[path_index].url}">{$special.path[path_index].title}</a></li>
      {/section}
    </ol>
  </nav>
{/if}
