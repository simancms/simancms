<!DOCTYPE html>
<html lang="en"><head>{$special.document.headstart}{$special.document.headdef}

	<link href="themes/{$special.theme}/assets/css/bootstrap.css" rel="stylesheet">
	<link href="themes/{$special.theme}/stylesheets.css" rel="stylesheet">

	<script src="themes/{$special.theme}/js/jquery.js"></script>
	<script src="themes/{$special.theme}/assets/js/bootstrap.bundle.js"></script>
	<script type="text/javascript" src="themes/{$special.theme}/script.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

{$_settings.meta_header_text}
{$special.document.headend}</head>
<body{$special.document.bodymodifier}>{$special.document.bodystart}
{if $_settings.header_static_text neq ""}{$_settings.header_static_text}{/if}