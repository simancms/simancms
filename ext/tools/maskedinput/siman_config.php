<?php

	//------------------------------------------------------------------------------
	//|            Content Management System SiMan CMS                             |
	//|              http://simancms.apserver.org.ua                               |
	//------------------------------------------------------------------------------

	if (!defined("maskedinput_FUNCTIONS_DEFINED"))
		{
			sm_add_jsfile('ext/tools/maskedinput/jquery.maskedinput.js', true);
			define("maskedinput_FUNCTIONS_DEFINED", 1);
		}
