<?php

	//------------------------------------------------------------------------------
	//|            Content Management System SiMan CMS                             |
	//|              http://simancms.apserver.org.ua                               |
	//------------------------------------------------------------------------------

	use SM\SM;

	if (defined("DATEPICKER_FUNCTIONS_DEFINED"))
		return;

	sm_add_cssfile('ext/tools/datepicker/bootstrap-datepicker.css', true);
	sm_add_jsfile('ext/tools/datepicker/bootstrap-datepicker.min.js', true, [], 'bodyend');

	function sm_tools_datepicker_include_lang($lang, $check_file_exists)
		{
			if ($check_file_exists && !file_exists(SM::ExternalLibsPublicPath('tools/datepicker/bootstrap-datepicker.'.$lang.'.min.js')))
				return;
			sm_add_jsfile('ext/tools/datepicker/bootstrap-datepicker.'.$lang.'.min.js', true, ['charset'=>'UTF-8'], 'bodyend');
		}

	if (!empty($sm['datepicker_language']) && $sm['datepicker_language']!='en')
		sm_tools_datepicker_include_lang($sm['datepicker_language'], true);
	elseif (empty($sm['datepicker_language']))
		{
			if (sm_current_language()=='ua' || sm_current_language()=='ukr')
				sm_tools_datepicker_include_lang('uk', false);
			else
				sm_tools_datepicker_include_lang(sm_current_language(), true);
		}

	define("DATEPICKER_FUNCTIONS_DEFINED", 1);
