<?php

	namespace SM;

	use SM\Common\ErrorCollector;
	use SM\Common\Input\RequestParam;
	use SM\Common\Input\RequestManager;

	class SM
		{
			protected static $userinfo=NULL;
			protected static $user=NULL;
			protected static $top_navigation=NULL;
			protected static $bottom_navigation=NULL;
			protected static $errors;

			static function isLoggedIn()
				{
					if (SM::$userinfo===NULL)
						return false;
					elseif (empty(SM::$userinfo['id']))
						return false;
					else
						return true;
				}

			static function isAdministrator()
				{
					if (!SM::isLoggedIn())
						return false;
					elseif (SM::$userinfo['id']==1)
						return true;
					else
						return SM::$userinfo['level']==3;
				}

		/**
		 * @return User
		 */
			static function User()
				{
					if (!SM::isLoggedIn())
						return new User([]);
					else
						return SM::$user;
				}

			static function initLoggedUser(&$userinfo)
				{
					SM::$userinfo=&$userinfo;
					SM::$user=new User($userinfo);
				}

		/**
		 * @return \SMNavigation
		 */
			static function TopNavigation()
				{
					global $sm;
					if (SM::$top_navigation===NULL)
						SM::$top_navigation=new \SMNavigation($sm['s']['uppermenu']);
					return SM::$top_navigation;
				}
		/**
		 * @return \SMNavigation
		 */
			static function BottomNavigation()
				{
					global $sm;
					if (SM::$bottom_navigation===NULL)
						SM::$bottom_navigation=new \SMNavigation($sm['s']['bottommenu']);
					return SM::$bottom_navigation;
				}

		/**
		 * @param string $param_name
		 * @return RequestParam
		 */
			static function POST($param_name)
				{
					global $sm;
					return new RequestParam($sm['p'], $param_name);
				}

		/**
		 * @param string $param_name
		 * @return RequestParam
		 */
			static function GET($param_name)
				{
					global $sm;
					return new RequestParam($sm['g'], $param_name);
				}

		/**
		 * @param string $param_name
		 * @return RequestParam
		 */
			static function Settings($param_name)
				{
					global $sm;
					return new RequestParam($sm['_s'], $param_name);
				}

		/**
		 * @param string $param_name
		 * @return RequestParam
		 */
			static function Session($param_name)
				{
					global $_sessionvars;
					return new RequestParam($_sessionvars, $param_name);
				}

		/**
		 * @return RequestManager
		 */
			static function Requests()
				{
					return new RequestManager();
				}

		/**
		 * @return ErrorCollector
		 */
			static function Errors()
				{
					if (!isset(self::$errors))
						self::$errors=new ErrorCollector();
					return self::$errors;
				}

		/**
		 * @param string $file_name
		 * @return string
		 */
			static function ModulesPath($file_name='')
				{
					return dirname(__FILE__, 3).'/modules/'.$file_name;
				}

		/**
		 * @param string $file_name
		 * @return string
		 */
			static function FilesPath($file_name='')
				{
					return dirname(__FILE__, 3).'/files/'.$file_name;
				}

		/**
		 * @param string $file_name
		 * @return string
		 */
			static function FilesURL($file_name='')
				{
					return 'files/'.$file_name;
				}

		/**
		 * @param string $file_name
		 * @return string
		 */
			static function TemporaryFilesPath($file_name='')
				{
					return self::FilesPath('temp/'.$file_name);
				}

		/**
		 * @param string $file_name
		 * @return string
		 */
			static function ThemesPath($file_name='')
				{
					return dirname(__FILE__, 3).'/themes/'.$file_name;
				}

		/**
		 * @param string $file_name
		 * @return string
		 */
			static function ExternalLibsPublicPath($file_name='')
				{
					return dirname(__FILE__, 3).'/ext/'.$file_name;
				}

		}