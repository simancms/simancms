<?php

	namespace SM\Themes;

	use SM\Common\Helpers\SMArray;
	use SM\Common\Helpers\SMStrings;
	use SM\Common\Helpers\ValueCopy;

	class CurrentTheme
		{

			public static function GetConfigVar($key_dot_notation)
				{
					global $sm;
					if (!isset($sm['current_theme_vars']))
						$sm['current_theme_vars']=[];
					if (SMStrings::isBeginningEqual('content_editor.', $key_dot_notation))
						$data=SMArray::Get($sm, 'contenteditor.'.SMStrings::RemoveBeginning('content_editor.', $key_dot_notation));
					if (SMStrings::isBeginningEqual('ui.buttons.', $key_dot_notation))
						$data=SMArray::Get($sm, 'adminbuttons.'.SMStrings::RemoveBeginning('ui.buttons.', $key_dot_notation));
					if (SMStrings::isBeginningEqual('ui.form.', $key_dot_notation))
						$data=SMArray::Get($sm, 'adminform.'.SMStrings::RemoveBeginning('ui.form.', $key_dot_notation));
					if (SMStrings::isBeginningEqual('ui.grid.', $key_dot_notation))
						$data=SMArray::Get($sm, 'admintable.'.SMStrings::RemoveBeginning('ui.grid.', $key_dot_notation));
					if (SMStrings::isBeginningEqual('ui.nav.', $key_dot_notation))
						$data=SMArray::Get($sm, 'adminnavigation.'.SMStrings::RemoveBeginning('ui.nav.', $key_dot_notation));
					if (SMStrings::isBeginningEqual('ui.tabs.', $key_dot_notation))
						$data=SMArray::Get($sm, 'admintabs.'.SMStrings::RemoveBeginning('ui.tabs.', $key_dot_notation));
					if (isset($data))
						return new ValueCopy($data);
					return new ValueCopy(SMArray::Get($sm['current_theme_vars'], $key_dot_notation));
				}

			public static function SetConfigVar($key_dot_notation, $value)
				{
					global $sm;
					if (!isset($sm['current_theme_vars']))
						$sm['current_theme_vars']=[];
					SMArray::Set($sm['current_theme_vars'], $key_dot_notation, $value);
				}

			public static function Name()
				{
					global $sm;
					return $sm['s']['theme'];
				}

		}