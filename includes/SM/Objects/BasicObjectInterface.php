<?php

	namespace SM\Objects;

	interface BasicObjectInterface
		{

			function ID();
			function Exists();

		}