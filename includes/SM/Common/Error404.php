<?php

	namespace SM\Common;

	use SM\Access\SMAccess;
	use SM\Objects\BasicObjectInterface;

	class Error404
		{

			public static function Display()
				{
					if (sm_is_main_block())
						SMAccess::EnforceError404();
					else
						sm_template('system_empty_block');
				}

			public static function DisplayIfEmpty($variable_or_object_with_exists)
				{
					if (
						isset($variable_or_object_with_exists)
						&& $variable_or_object_with_exists instanceof BasicObjectInterface
					)
						$error=!$variable_or_object_with_exists->Exists();
					else
						$error=empty($variable_or_object_with_exists);
					if ($error)
						self::Display();
				}

		}
