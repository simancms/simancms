<?php

	namespace SM\Common;

	use SM\UI\UI;

	class MessageCollector
		{

			private $messages=[];

			const ERROR='error';

			public function Count()
				{
					return count($this->messages);
				}

			public function AddError($message)
				{
					$this->messages[]=[
						'message'=>(string)$message,
						'type'=>self::ERROR,
					];
				}

		/**
		 * @param UI $ui
		 * @return void
		 */
			public function DisplayUIErrors($ui)
				{
					$errors=[];
					foreach ($this->messages as $msg_arr)
						{
							if ($msg_arr['type']===self::ERROR)
								$errors[]=$msg_arr['message'];
						}
					if (count($errors)>0)
						{
							$ui->NotificationError(implode('. ', $errors));
						}
				}

		}
