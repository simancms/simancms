<?php

	namespace SM\Common;

	use SM\UI\UI;

	class ErrorCollector extends MessageCollector
		{

			public function AddFillRequiredFieldsError()
				{
					$this->AddError(sm_lang('messages.fill_required_fields'));
				}

		}
