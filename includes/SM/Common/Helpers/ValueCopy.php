<?php


	namespace SM\Common\Helpers;

	use SM\Core\ValueAccessPrototype;

	class ValueCopy extends ValueAccessPrototype
		{
			private $value;

			function __construct($value)
				{
					$this->value=$value;
				}

			protected function RawValue()
				{
					return $this->value;
				}

			function SetValue($new_value)
				{
					$this->value=$new_value;
				}

		}
