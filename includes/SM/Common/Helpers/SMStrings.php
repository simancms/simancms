<?php

	namespace SM\Common\Helpers;

	class SMStrings
		{

			public static function isEqual($str1, $str2)
				{
					return strcmp((string)$str1, (string)$str2)==0;
				}

			public static function isBeginningEqual($beginning, $full_string)
				{
					if (strlen((string)$full_string)<strlen((string)$beginning))
						return false;
					else
						return self::isEqual((string)$beginning, substr((string)$full_string, 0, strlen($beginning)));
				}

			public static function RemoveBeginning($beginning, $full_string)
				{
					if (!self::isBeginningEqual($beginning, $full_string))
						return $full_string;
					else
						return substr($full_string, strlen($beginning));
				}

			public static function ReplaceBeginning($beginning, $replace, $full_string)
				{
					return $replace.self::RemoveBeginning($beginning, $full_string);
				}

		}