<?php

	namespace SM\Common\Helpers;

	class SMArray
		{

			public static function Get(&$array, $key_dot_notation)
				{
					$keys=explode('.', $key_dot_notation);
					$data=&$array;
					while (count($keys)>0)
						{
							$key=array_shift($keys);
							if (isset($data[$key]))
								$data=&$data[$key];
							else
								return NULL;
						}
					return $data;
				}

			public static function Set(&$array, $key_dot_notation, $value)
				{
					$keys=explode('.', $key_dot_notation);
					$data=&$array;
					while (count($keys)>0)
						{
							$key=array_shift($keys);
							if (!isset($data[$key]))
								{
									$data[$key]=NULL;
								}
							$data=&$data[$key];
						}
					$data=$value;
				}

			public static function FilterKeys($keys_array_to_filter, $key_val_arr)
				{
					$filtered=[];
					foreach ($keys_array_to_filter as $key)
						{
							if (isset($key_val_arr[$key]))
								$filtered[$key]=$key_val_arr[$key];
						}
					return $filtered;
				}

		}