<?php


	namespace SM\Common\Helpers;

	use SM\Core\ValueAccessPrototype;

	class HTMLGenerator
		{

			public static function AttributesFromArray($attributes_key_val_array)
				{
					$attrs=[];
					foreach ($attributes_key_val_array as $key=>$val)
						{
							if (is_array($val))
								$val=implode(' ', $val);
							if (strlen($val)===0)
								continue;
							$attrs[]=$key.'="'.htmlescape($val).'"';
						}
					return implode(' ', $attrs);
				}

		}
