<?php


	namespace SM\Common\Input;

	use SM\Core\ValueAccessPrototype;

	class RequestParam extends ValueAccessPrototype
		{
			private $current_request;
			private $param_name;

			function __construct(&$request_array, $request_param_name)
				{
					$this->current_request=&$request_array;
					$this->param_name=$request_param_name;
				}

			protected function RawValue()
				{
					return isset($this->current_request[$this->param_name]) ? $this->current_request[$this->param_name] : NULL;
				}

			function Exists()
				{
					return array_key_exists($this->param_name, $this->current_request);
				}

			function SetValue($new_value)
				{
					$this->current_request[$this->param_name]=$new_value;
				}

		}
