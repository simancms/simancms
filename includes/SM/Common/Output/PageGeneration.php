<?php

	namespace SM\Common\Output;

	class PageGeneration
		{

			public static function SetPrintMode()
				{
					global $special;
					$special['printmode'] = 'on';
					$special['main_tpl'] = 'indexprint';
				}

			public static function SetOutputMainBlockOnly()
				{
					global $special, $modules, $modules_index;
					$special['main_tpl']='theonepage';
					$special['no_blocks']=true;
					$modules[$modules_index]['borders_off']=true;
				}

		}
