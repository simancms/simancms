<?php

	namespace SM\Access;

	use SM\SM;

	class SMAccess
		{

			private static function ThrowException($exception=NULL)
				{
					if ($exception===NULL)
						$exception=new SMAccessDeniedException();
					throw $exception;
				}

			public static function LoginRequired()
				{
					if (!SM::isLoggedIn())
						self::ThrowException();
				}

			public static function GenericAccessLevelRequired($access_level)
				{
					if ($access_level===0)
						return;
					if (!SM::isLoggedIn())
						self::ThrowException();
					elseif (SM::User()->Level()<$access_level)
						self::ThrowException();
				}

			public static function EnforceLogin()
				{
					if (!SM::isLoggedIn())
						{
							$e=new SMAccessDeniedException();
							$e->enforce_login=true;
							self::ThrowException($e);
						}
				}

			public static function EnforceError404()
				{
					$e=new SMAccessDeniedException();
					$e->error404=true;
					self::ThrowException($e);
				}

			public static function Error404IfEmpty($objectwithexists_or_scalar)
				{
					if (is_object($objectwithexists_or_scalar) && $objectwithexists_or_scalar->Exists())
						return;
					elseif (!empty($objectwithexists_or_scalar))
						return;
					static::EnforceError404();
				}

		}