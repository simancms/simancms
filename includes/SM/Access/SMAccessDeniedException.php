<?php

	namespace SM\Access;

	use Exception;

	class SMAccessDeniedException extends Exception
		{

			public $enforce_login=false;
			public $error404=false;

		}