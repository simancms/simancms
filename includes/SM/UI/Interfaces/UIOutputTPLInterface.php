<?php

	namespace SM\UI\Interfaces;

	interface UIOutputTPLInterface
		{

			public function TPLDataArray();
			public function TPLName();

		}