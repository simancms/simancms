<?php

	namespace SM\UI;

	use SM\SM;
	use SM\Themes\CurrentTheme;

	sm_add_cssfile('common_adminform.css');

	class Form
		{
			private $form;
			private $firsteditor = true;
			private $currentname;
			private $query_to_hidden_fields=false;

			static $forms_used=0;
			static $editor_instances=0;
		/**
		 * @var true
		 */
			private $disable_autofill_all_fields=false;

			function __construct($action='', $prefix = '', $method = 'POST')
				{
					$this->form['action'] = $action;
					if ($action === false)
						$this->form['dont_use_form_tag'] = 1;
					$this->form['method'] = $method;
					$this->form['prefix'] = $prefix;
					$this->form['postfix'] = Form::$forms_used;
					$this->form['tooltip_present'] = false;
					$this->form['class'] = '';
					$this->form['files'] = '';
					$this->form['rowclassname'] = '';
					$this->form['fields'] = [];
					$this->form['no_highlight'] = 0;
					$this->NoHighlight(CurrentTheme::GetConfigVar('ui.form.nohighlight')->AsBool());
					$this->AddClassnameGlobal('adminform_form');
					if (!CurrentTheme::GetConfigVar('ui.form.globalclass')->isEmpty())
						$this->AddClassnameGlobal(CurrentTheme::GetConfigVar('ui.form.globalclass')->AsString());
					$this->SetFormAttribute('enctype', 'multipart/form-data');
					$this->SetDOMID('uiform-'.Form::$forms_used);
					Form::$forms_used++;
					$this->AfterConstruct();
				}

			protected function AfterConstruct()
				{}

			public static function withAction($action, $method = 'POST')
				{
					return new Form($action, '', $method);
				}

			function SetDOMID($id)
				{
					$this->SetFormAttribute('id', $id);
				}

			function GetDOMID()
				{
					return $this->GetFormAttribute('id');
				}

			function SetMethodGet($query_to_hidden_fields=false)
				{
					$this->form['method'] = 'GET';
					$this->query_to_hidden_fields=$query_to_hidden_fields;
					return $this;
				}

			function SetMethodPost()
				{
					$this->form['method'] = 'POST';
					return $this;
				}

			function SetAction($action)
				{
					$this->form['action'] = $action;
					return $this;
				}

			function AddSeparator($name, $title)
				{
					$this->InitField($name, self::FieldTypeTagSeparator());
					$this->form['fields'][$name]['caption'] = $title;
					$this->SetRowClass('adminform-separator');
					return $this;
				}

			function Separator($title)
				{
					$name = 'separator'.count($this->form['fields']).'r'.rand(1111, 9999);
					$this->AddSeparator($name, $title);
					return $this;
				}

			function SetRowClass($class, $name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->form['fields'][$name]['rowclassname'] = $class;
					return $this;
				}

			function AppendRowClass($class, $name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->form['fields'][$name]['rowclassname'] .= (sm_strlen($this->form['fields'][$name]['rowclassname']) == 0 ? '' : ' ').$class;
					return $this;
				}

			private function SetFieldType($name, $type)
				{
					$this->form['fields'][$name]['type'] = $type;
				}

			private function InitField($name, $type='')
				{
					$this->currentname = $name;
					$this->form['fields'][$name]['name'] = $name;
					$this->form['fields'][$name]['hidedefinition'] = 0;
					$this->form['fields'][$name]['rowclassname'] = '';
					$this->form['fields'][$name]['caption'] = '';
					$this->form['fields'][$name]['type'] = '';
					$this->form['fields'][$name]['attrs']['class'] = '';
					$this->form['fields'][$name]['column'][1] = '';
					$this->form['fields'][$name]['column'][2] = '';
					if (!empty($type))
						$this->SetFieldType($name, $type);
				}

			function AddLabel($name, $title='', $labeltext='')
				{
					$this->InitField($name, self::FieldTypeTagLabel());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['labeltext'] = $labeltext;
					return $this;
				}

			function Label($title, $labeltext)
				{
					$name='tmpfrmlbl'.count($this->form['fields']).'-'.md5(microtime());
					$this->AddLabel($name, $title, $labeltext);
					return $this;
				}

			function AddText($name, $title='', $required = false)
				{
					$this->InitField($name, self::FieldTypeTagText());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['required'] = $required;
					if (!CurrentTheme::GetConfigVar('ui.form.textclass')->isEmpty())
						$this->SetFieldClass($name, CurrentTheme::GetConfigVar('ui.form.textclass')->AsString());
					return $this;
				}

			function AddPassword($name, $title='', $required = false)
				{
					$this->InitField($name, self::FieldTypeTagPassword());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['required'] = $required;
					return $this;
				}

			function AddFile($name, $title='', $required = false)
				{
					$this->InitField($name, self::FieldTypeTagFile());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['required'] = $required;
					$this->form['files'] = addto_nllist($this->form['files'], $name);
					return $this;
				}

			function AddStatictext($name, $title='', $required = false)
				{
					$this->InitField($name, self::FieldTypeTagStaticText());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['required'] = $required;
					return $this;
				}

			function AddHidden($name, $value = '')
				{
					$this->InitField($name, self::FieldTypeTagHidden());
					$this->SetValue($name, $value);
					return $this;
				}

			function AddSystemHidden($name, $value = '')
				{
					$this->InitField($name, self::FieldTypeTagHidden());
					$this->SetValue($name, $value);
					return $this;
				}

			function AddTextarea($name, $title='', $required = false)
				{
					$this->InitField($name, self::FieldTypeTagTextarea());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['required'] = $required;
					if (!CurrentTheme::GetConfigVar('ui.form.textareaclass')->isEmpty())
						$this->SetFieldClass($name, CurrentTheme::GetConfigVar('ui.form.textareaclass')->AsString());
					return $this;
				}

			function AddOutputObject($type, $object, $tpl = '')
				{
					$this->form['fields'][$this->currentname]['type'] = $type;
					$this->form['fields'][$this->currentname]['tpl'] = $tpl;
					$this->form['fields'][$this->currentname]['data'] = $object->Output();
					return $this;
				}

		/**
		 * @param Buttons $buttons
		 * @param string|NULL $title
		 * @param string|NULL $name
		 * @return $this
		 */
			function InsertButtons($buttons, $title = NULL, $name = NULL)
				{
					if ($name == NULL)
						$name = 'buttons_'.count($this->form['fields']).'_'.rand(1, 999999);
					$this->InitField($name);
					$buttons->AddClassnameGlobal('adminformbuttons');
					$this->AddOutputObject('bar', $buttons);
					if ($title == NULL)
						$this->MergeColumns();
					return $this;
				}

			function InsertGrid($grid, $title = NULL, $name = NULL)
				{
					if ($name == NULL)
						$name = 'table_'.count($this->form['fields']).'_'.rand(1, 999999);
					$this->InitField($name);
					$this->AddOutputObject('table', $grid);
					if ($title == NULL)
						$this->MergeColumns();
					return $this;
				}

			function InsertHTML($html, $title = NULL, $name = NULL)
				{
					if ($name == NULL)
						$name = 'buttons_'.count($this->form['fields']).'_'.rand(1, 999999);
					$this->InitField($name, self::FieldTypeTagPlainHTML());
					$this->form['fields'][$this->currentname]['html'] = $html;
					if ($title == NULL)
						$this->MergeColumns();
					return $this;
				}

			function InsertTPL($tpl, $data = [], $action = '', $title = NULL, $name = NULL)
				{
					if ($name == NULL)
						$name = 'buttons_'.count($this->form['fields']).'_'.rand(1, 999999);
					$this->InitField($name, self::FieldTypeTagTPL());
					$this->form['fields'][$this->currentname]['tpl'] = $tpl;
					$this->form['fields'][$this->currentname]['data'] = $data;
					$this->form['fields'][$this->currentname]['action'] = $action;
					if ($title == NULL)
						$this->MergeColumns();
					return $this;
				}

			function AddEditor($name, $title='', $required = false)
				{
					$this->InitField($name, self::FieldTypeTagWYSIWIGEditor());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['required'] = $required;
					if (!$this->firsteditor || $this::$editor_instances>1)
						$this->form['fields'][$name]['noinit'] = 1;
					if ($this->firsteditor)
						$this->firsteditor = false;
					$this::$editor_instances++;
					return $this;
				}

			function AddCheckbox($name, $title='', $checkedvalue = 1, $required = false)
				{
					$this->InitField($name, Form::FieldTypeTagCheckbox());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['required'] = $required;
					$this->form['fields'][$name]['checkedvalue'] = $checkedvalue;
					return $this;
				}

			function AddRadioGroup($name, $title='', $array_values=[], $array_labels=[], $required = false)
				{
					$this->InitField($name, self::FieldTypeTagRadioGroup());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['required'] = $required;
					$this->form['fields'][$name]['values'] = $array_values;
					$this->form['fields'][$name]['labels'] = $array_labels;
					if (!CurrentTheme::GetConfigVar('ui.form.radiogroup_class')->isEmpty())
						$this->SetFieldClass($name, CurrentTheme::GetConfigVar('ui.form.radiogroup_class')->AsString());
					return $this;
				}

		/*
			AddSelect($name, $title='', $array_values=[], $required = false)
			AddSelect($name, $title='', $array_values=[], $array_labels=[], $required = false)
		*/
			function AddSelect($name, $title='', $array_values=[], $array_labels=[], $required = false)
				{
					//backward compatibility parameters
					if (is_array($array_values) && is_bool($array_labels))
						{
							$required = $array_labels;
							$array_labels=$array_values;
						}
					elseif (is_array($array_values) && is_array($array_labels) && count($array_labels)===0)
						{
							$array_labels=$array_values;
						}
					if (!is_array($array_values))
						$array_values=[];
					if (!is_array($array_labels))
						$array_labels=[];
					$this->InitField($name, self::FieldTypeTagSelect());
					$this->form['fields'][$name]['caption'] = $title;
					$this->form['fields'][$name]['required'] = $required;
					$this->form['fields'][$name]['values'] = $array_values;
					$this->form['fields'][$name]['labels'] = $array_labels;
					if (!CurrentTheme::GetConfigVar('ui.form.selectclass')->isEmpty())
						$this->SetFieldClass($name, CurrentTheme::GetConfigVar('ui.form.selectclass')->AsString());
					return $this;
				}

		/**
		 * @deprecated
		 */
			function AddSelectVL($name, $title='', $array_values=[], $array_labels=[], $required = false)
				{
					$this->AddSelect($name, $title, $array_values, $array_labels, $required);
					return $this;
				}

		/**
		 * @deprecated
		 */
			function AddSelectNLListVL($name, $title='', $nllist_values='', $nllist_labels='', $required = false)
				{
					$this->AddSelect($name, $title, nllistToArray($nllist_values), nllistToArray($nllist_labels), $required);
					return $this;
				}

			function SelectAddBeginVL($name, $value, $label)
				{
					if (isset($this->form['fields'][$name]['values']))
						{
							array_unshift($this->form['fields'][$name]['values'], $value);
							array_unshift($this->form['fields'][$name]['labels'], $label);
						}
					else
						{
							$this->form['fields'][$name]['values'] = [$value];
							$this->form['fields'][$name]['labels'] = [$label];
						}
					return $this;
				}

			function SelectAddEndVL($name, $value, $label)
				{
					if (isset($this->form['fields'][$name]['values']))
						{
							$this->form['fields'][$name]['values'][]=$value;
							$this->form['fields'][$name]['labels'][]=$label;
						}
					else
						{
							$this->form['fields'][$name]['values'] = [$value];
							$this->form['fields'][$name]['labels'] = [$label];
						}
					return $this;
				}

			function GetTitle($name)
				{
					if (!$this->HasField($name))
						return '';
					return $this->form['fields'][$name]['caption'];
				}

			function GetType($name)
				{
					if (!$this->HasField($name))
						return '';
					return $this->form['fields'][$name]['type'];
				}

			function HasField($name)
				{
					return isset($this->form['fields'][$name]);
				}

			function GetFieldNames()
				{
					return array_keys($this->form['fields']);
				}

			function SetValue($name, $value)
				{
					if ($this->GetType($name)==Form::FieldTypeTagLabel())
						{
							if (!is_array($value))
								$this->form['fields'][$name]['labeltext']=htmlescape($value);
							else
								$this->form['fields'][$name]['labeltext']=$value;
						}
					elseif ($this->GetType($name)==Form::FieldTypeTagCheckbox() && is_bool($value))
						{
							if ($value)
								$this->SetValue($name, $this->form['fields'][$name]['checkedvalue']);
							else
								$this->SetValue($name, '');
						}
					else
						{
							if (!is_array($value))
								$this->form['data'][$name]=htmlescape($value);
							else
								$this->form['data'][$name]=$value;
						}
					return $this;
				}

			function GetValue($name)
				{
					return $this->form['data'][$name];
				}

			function SetNotEscapedValue($name, $value)
				{
					$this->form['data'][$name] = $value;
				}

			function ToggleFor($element_name_or_array, $name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					if (is_array($element_name_or_array))
						{
							foreach ($element_name_or_array as $key=>$val)
								$this->ToggleFor($val, $name);
						}
					else
						$this->form['fields'][$name]['checkbox_toggle'][] = $element_name_or_array;
					return $this;
				}

			function ValueToggleFor($element_name_or_array, $value_on, $name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					if (is_array($element_name_or_array))
						{
							foreach ($element_name_or_array as $val)
								$this->ValueToggleFor($val, $value_on, $name);
						}
					else
						$this->form['fields'][$name]['value_toggle'][] = ['id'=>$element_name_or_array, 'val'=>$value_on];
					return $this;
				}

			protected function BeforeOutput()
				{}

			private function QueryToHiddenFieldsBeforeOutput()
				{
					if (strpos($this->form['action'], '?')!==false)
						{
							$tmp=explode('?', $this->form['action']);
							if (count($tmp)==2)
								{
									parse_str($tmp[1], $data);
									$this->form['action']=$tmp[0];
									foreach ($data as $key=>$val)
										{
											if (!$this->HasField($key))
												$this->AddHidden($key, $val);
										}
								}
						}
				}

			function Output()
				{
					if ($this->disable_autofill_all_fields)
						{
							foreach ($this->GetFieldNames() as $name)
								{
									$this->DisableAutofill($name);
								}
						}
					$this->BeforeOutput();
					if ($this->query_to_hidden_fields)
						$this->QueryToHiddenFieldsBeforeOutput();
					$this->form['method'] = strtolower($this->form['method']);
					if (is_array($this->form['fields']))
						{
							if ($this->form['no_highlight'] != 1)
								{
									$class = '';
									foreach ($this->form['fields'] as $name => $value)
										{
											if ($this->form['fields'][$name]['hidedefinition'] == 1 || $this->form['fields'][$name]['type'] == 'separator')
												continue;
											if ($class != 'adminform-row-odd')
												$class = 'adminform-row-odd';
											else
												$class = 'adminform-row-pair';
											$this->AppendRowClass($class, $name);
										}
								}
							foreach ($this->form['fields'] as $name => $value)
								{
									if (!CurrentTheme::GetConfigVar('ui.form.rowclass')->isEmpty())
										$this->AppendRowClass(CurrentTheme::GetConfigVar('ui.form.rowclass')->AsString(), $name);
									$this->SetFieldId($name, $this->GetFieldId($name));
									if (!empty($this->form['fields'][$name]['rowclassname']))
										$this->AppendFieldRowAttribute($name, 'class', $this->form['fields'][$name]['rowclassname']);
									$this->SetFieldRowAttribute($name, 'id', $this->GetFieldRowId($name));
									if (!empty($this->form['fields'][$name]['toptext']))
										$this->form['fields'][$name]['toptext'] = '<span class="adminform-filed-top-txt'.(!empty($this->form['fields'][$name]['toptext_classname']) ? ' '.$this->form['fields'][$name]['toptext_classname'] : '').'"'.(!empty($this->form['fields'][$name]['toptext_style']) ? ' style="'.$this->form['fields'][$name]['toptext_style'].'"' : '').'>'.$this->form['fields'][$name]['toptext'].'</span>';
									if (!empty($this->form['fields'][$name]['bottomtext']))
										$this->form['fields'][$name]['bottomtext'] = '<span class="adminform-filed-btm-txt'.(!empty($this->form['fields'][$name]['bottomtext_classname']) ? ' '.$this->form['fields'][$name]['bottomtext_classname'] : '').'"'.(!empty($this->form['fields'][$name]['bottomtext_style']) ? ' style="'.$this->form['fields'][$name]['bottomtext_style'].'"' : '').'>'.$this->form['fields'][$name]['bottomtext'].'</span>';
									if (!empty($this->form['fields'][$name]['begintext']))
										$this->form['fields'][$name]['begintext'] = '<span class="adminform-filed-bgn-txt'.(!empty($this->form['fields'][$name]['begintext_classname']) ? ' '.$this->form['fields'][$name]['begintext_classname'] : '').'"'.(!empty($this->form['fields'][$name]['begintext_style']) ? ' style="'.$this->form['fields'][$name]['begintext_style'].'"' : '').'>'.$this->form['fields'][$name]['begintext'].'</span>';
									if (!empty($this->form['fields'][$name]['endtext']))
										$this->form['fields'][$name]['endtext'] = '<span class="adminform-filed-end-txt'.(!empty($this->form['fields'][$name]['endtext_classname']) ? ' '.$this->form['fields'][$name]['endtext_classname'] : '').'"'.(!empty($this->form['fields'][$name]['endtext_style']) ? ' style="'.$this->form['fields'][$name]['endtext_style'].'"' : '').'>'.$this->form['fields'][$name]['endtext'].'</span>';
									if (!empty($this->form['fields'][$name]['tooltip']) || !empty($this->form['fields'][$name]['tooltip_url']))
										{
											$this->form['fields'][$name]['column'][2] = '<div class="adminform-tooltip" title="'.$this->form['fields'][$name]['tooltip'].'">';
											if (!empty($this->form['fields'][$name]['tooltip_url']))
												$this->form['fields'][$name]['column'][2] .= '<a href="'.$this->form['fields'][$name]['tooltip_url'].'"'.(!empty($this->form['fields'][$name]['tooltip_url_target'])?' target="'.$this->form['fields'][$name]['tooltip_url_target'].'"':'').' class="tooltip-url">';
											$this->form['fields'][$name]['column'][2] .= '<img src="'.$this->form['fields'][$name]['tooltipimg'].'" />';
											if (!empty($this->form['fields'][$name]['tooltip_url']))
												$this->form['fields'][$name]['column'][2] .= '</a>';
											$this->form['fields'][$name]['column'][2] .= '</div>';
										}
									if ($this->form['fields'][$name]['type'] == self::FieldTypeTagSelect())
										{
											for ($i = 0; $i < count($this->form['fields'][$name]['values']); $i++)
												{
													if (sm_strlen($this->form['fields'][$name]['labels'][$i])==0)
														$this->form['fields'][$name]['options'][$i]['label']=htmlescape($this->form['fields'][$name]['values'][$i]);
													else
														$this->form['fields'][$name]['options'][$i]['label']=htmlescape($this->form['fields'][$name]['labels'][$i]);
													$this->form['fields'][$name]['options'][$i]['attrs']['value']=htmlescape($this->form['fields'][$name]['values'][$i]);
													if (isset($this->form['data'][$name]))
														{
															if (is_array($this->form['data'][$name]))
																{
																	if (in_array($this->form['fields'][$name]['values'][$i], $this->form['data'][$name]))
																		$this->form['fields'][$name]['options'][$i]['attrs']['selected']='selected';
																}
															else
																{
																	if (sm_strcmp($this->form['data'][$name], $this->form['fields'][$name]['values'][$i])==0)
																		$this->form['fields'][$name]['options'][$i]['attrs']['selected']='selected';
																}
														}
												}
										}
									if ($this->form['fields'][$name]['type'] == self::FieldTypeTagRadioGroup())
										{
											for ($i = 0; $i < count($this->form['fields'][$name]['values']); $i++)
												{
													if (sm_strlen($this->form['fields'][$name]['labels'][$i])==0)
														$this->form['fields'][$name]['options'][$i]['label']=htmlescape($this->form['fields'][$name]['values'][$i]);
													else
														$this->form['fields'][$name]['options'][$i]['label']=htmlescape($this->form['fields'][$name]['labels'][$i]);
													$this->form['fields'][$name]['options'][$i]['attrs']['value']=htmlescape($this->form['fields'][$name]['values'][$i]);
													if (is_array($this->form['data'][$name]))
														{
															if (in_array($this->form['fields'][$name]['values'][$i], $this->form['data'][$name]))
																$this->form['fields'][$name]['options'][$i]['attrs']['checked']='checked';
														}
													else
														{
															if (sm_strcmp($this->form['data'][$name], $this->form['fields'][$name]['values'][$i])==0)
																$this->form['fields'][$name]['options'][$i]['attrs']['checked']='checked';
														}
													if (!isset($this->form['fields'][$name]['options_label'][$i]['attrs']))
														$this->form['fields'][$name]['options_label'][$i]['attrs']=[];
													if (!isset($this->form['fields'][$name]['options_item'][$i]['attrs']))
														$this->form['fields'][$name]['options_item'][$i]['attrs']=[
															'class'=>'',
														];
													$this->form['fields'][$name]['options_item'][$i]['attrs']['class'].=
														(sm_strlen($this->form['fields'][$name]['options_item'][$i]['attrs']['class'])==0?'':' ')
														.'adminform-radiogroup-item adminform-radiogroup-item-'.$i.' '.$name.'-adminform-radiogroup-item'.' '.$name.'-adminform-radiogroup-item-'.$i;
												}
										}
									if (isset($this->form['fields'][$name]['checkbox_toggle']) && is_array($this->form['fields'][$name]['checkbox_toggle']))
										for ($i = 0; $i<count($this->form['fields'][$name]['checkbox_toggle']); $i++)
											{
												$this->javascriptCode('$("#'.$this->GetFieldId($name).'").change(function(){if($("#'.$this->GetFieldId($name).'").prop("checked"))$("#'.$this->GetFieldRowId($this->form['fields'][$name]['checkbox_toggle'][$i]).'").show();else $("#'.$this->GetFieldRowId($this->form['fields'][$name]['checkbox_toggle'][$i]).'").hide();});$("#'.$this->GetFieldId($name).'").change();');
											}
									if (isset($this->form['fields'][$name]['value_toggle']) && is_array($this->form['fields'][$name]['value_toggle']))
										for ($i = 0; $i<count($this->form['fields'][$name]['value_toggle']); $i++)
											{
												if (!is_array($this->form['fields'][$name]['value_toggle'][$i]['val']))
													$tmp='"'.jsescape($this->form['fields'][$name]['value_toggle'][$i]['val']).'"';
												else
													{
														$tmp='';
														for ($j = 0; $j<count($this->form['fields'][$name]['value_toggle'][$i]['val']); $j++)
															{
																if (!empty($tmp))
																	$tmp.=',';
																$tmp.='"'.jsescape($this->form['fields'][$name]['value_toggle'][$i]['val'][$j]).'"';
															}
													}
												$this->javascriptCode('$("#'.$this->GetFieldId($name).'").change(function(){if($.inArray($("#'.$this->GetFieldId($name).'").val(), ['.$tmp.'])!=-1)$("#'.$this->GetFieldRowId($this->form['fields'][$name]['value_toggle'][$i]['id']).'").show();else $("#'.$this->GetFieldRowId($this->form['fields'][$name]['value_toggle'][$i]['id']).'").hide();});$("#'.$this->GetFieldId($name).'").change();');
											}
								}
						}
					if (!empty($this->form['class']))
						$this->SetFormAttribute('class', $this->form['class']);
					if (!empty($this->form['action']))
						$this->SetFormAttribute('action', $this->form['action']);
					if (!empty($this->form['method']))
						$this->SetFormAttribute('method', $this->form['method']);
					else
						$this->SetFormAttribute('method', 'post');
					return $this->form;
				}

		//-------------------------------------------------------------
			function LoadValuesArray($array)
				{
					if (!is_array($array))
						return $this;
					foreach ($array as $name=>$value)
						{
							$this->SetValue($name, $value);
						}
					return $this;
				}

			function LoadAllValues($array)
				{
					if (!is_array($array) || count($array)==0)
						return $this;
					if (!is_array($this->form['fields']))
						return $this;
					foreach ($this->form['fields'] as $name => $value)
						{
							$this->SetValue($name, $array[$name]);
						}
					return $this;
				}

			function SetColumnsWidth($first, $second)
				{
					$this->form['options']['width1'] = $first;
					$this->form['options']['width2'] = $second;
					return $this;
				}

		//-------------------------------------------------------------
			function SetFormAttribute($attribute, $value)
				{
					if ($value===NULL)
						unset($this->form['attrs'][$attribute]);
					else
						$this->form['attrs'][$attribute] = $value;
					return $this;
				}

			function AppendFormAttribute($attribute, $value, $delimiter=' ')
				{
					$attrval=$this->GetFormAttribute($attribute);
					if (!empty($attrval))
						$attrval.=$delimiter;
					$attrval.=$value;
					$this->SetFormAttribute($attribute, $attrval);
					return $this;
				}

			function UnsetFormAttribute($attribute)
				{
					$this->SetFormAttribute($attribute, NULL);
					return $this;
				}

			function GetFormAttribute($attribute)
				{
					return $this->form['attrs'][$attribute];
				}

			function HasFormAttribute($attribute)
				{
					return is_array($this->form['attrs']) && array_key_exists($attribute, $this->form['attrs']);
				}

		//-------------------------------------------------------------
			function SetFieldId($name, $id)
				{
					$this->form['fields'][$name]['id'] = $id;
					return $this;
				}

			function GetFieldId($name)
				{
					if (!empty($this->form['fields'][$name]['id']))
						return $this->form['fields'][$name]['id'];
					else
						return $this->form['prefix'].$name;
				}

			function GetFieldRowId($name)
				{
					return 'admintablerow-'.$this->GetFieldId($name);
				}

			function SetFieldAttribute($name, $attribute, $value)
				{
					if ($name === NULL)
						$name = $this->currentname;
					if ($value===NULL)
						unset($this->form['fields'][$name]['attrs'][$attribute]);
					else
						$this->form['fields'][$name]['attrs'][$attribute] = $value;
					return $this;
				}

			function WithFieldAttribute($attribute, $value, $name=NULL)
				{
					$this->SetFieldAttribute($name, $attribute, $value);
					return $this;
				}

			function UnsetFieldAttribute($attribute, $name=NULL)
				{
					$this->SetFieldAttribute($name, $attribute, NULL);
					return $this;
				}

			function GetFieldAttribute($name, $attribute)
				{
					return $this->form['fields'][$name]['attrs'][$attribute];
				}

			function HasFieldAttribute($name, $attribute)
				{
					return is_array($this->form['fields'][$name]['attrs']) && array_key_exists($attribute, $this->form['fields'][$name]['attrs']);
				}

			function SetFieldRowAttribute($name, $attribute, $value)
				{
					if ($name === NULL)
						$name = $this->currentname;
					if ($value===NULL)
						unset($this->form['fields'][$name]['rowattrs'][$attribute]);
					else
						$this->form['fields'][$name]['rowattrs'][$attribute] = $value;
					return $this;
				}

			function WithFieldRowAttribute($attribute, $value, $name=NULL)
				{
					$this->SetFieldRowAttribute($name, $attribute, $value);
					return $this;
				}

			function GetFieldRowAttribute($name, $attribute)
				{
					if (isset($this->form['fields'][$name]['rowattrs'][$attribute]))
						return $this->form['fields'][$name]['rowattrs'][$attribute];
					else
						return '';
				}

			function AppendFieldRowAttribute($name, $attribute, $value, $delimiter=' ')
				{
					$attr=$this->GetFieldRowAttribute($name, $attribute);
					if (!empty($attr))
						$attr.=$delimiter;
					$attr.=$value;
					$this->SetFieldRowAttribute($name, $attribute, $attr);
				}

			function SetTitleText($name, $title)
				{
					$this->form['fields'][$name]['caption'] = $title;
					return $this;
				}

			function WithTitle($title)
				{
					$this->SetTitleText($this->currentname, $title);
					return $this;
				}

			function SetFieldTopText($name, $text, $classname = '', $style = '')
				{
					$this->form['fields'][$name]['toptext'] = $text;
					$this->form['fields'][$name]['toptext_classname'] = $classname;
					$this->form['fields'][$name]['toptext_style'] = $style;
					return $this;
				}

			function WithFieldTopText($text, $classname = '', $style = '')
				{
					$this->SetFieldTopText($this->currentname, $text, $classname, $style);
					return $this;
				}

			function SetFieldBeginText($name, $text, $classname = '', $style = '')
				{
					$this->form['fields'][$name]['begintext'] = $text;
					$this->form['fields'][$name]['begintext_classname'] = $classname;
					$this->form['fields'][$name]['begintext_style'] = $style;
					return $this;
				}

			function WithFieldBeginText($text, $classname = '', $style = '')
				{
					$this->SetFieldBeginText($this->currentname, $text, $classname, $style);
					return $this;
				}

			function SetFieldEndText($name, $text, $classname = '', $style = '')
				{
					$this->form['fields'][$name]['endtext'] = $text;
					$this->form['fields'][$name]['endtext_classname'] = $classname;
					$this->form['fields'][$name]['endtext_style'] = $style;
					return $this;
				}

			function WithFieldEndText($text, $classname = '', $style = '')
				{
					$this->SetFieldEndText($this->currentname, $text, $classname, $style);
					return $this;
				}

			function SetFieldBottomText($name, $text, $classname = '', $style = '')
				{
					$this->form['fields'][$name]['bottomtext'] = $text;
					$this->form['fields'][$name]['bottomtext_classname'] = $classname;
					$this->form['fields'][$name]['bottomtext_style'] = $style;
					return $this;
				}

			function WithFieldBottomText($text, $classname = '', $style = '')
				{
					$this->SetFieldBottomText($this->currentname, $text, $classname, $style);
					return $this;
				}

			function MergeColumns($name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->form['fields'][$name]['mergecolumns'] = 1;
					return $this;
				}

			function HideDefinition($name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->form['fields'][$name]['hidedefinition'] = 1;
					return $this;
				}

			function HideEncloser($name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->form['fields'][$name]['hideencloser'] = 1;
					return $this;
				}

			function SetImage($name, $src, $href = '')
				{
					$this->form['fields'][$name]['image']['src'] = $src;
					$this->form['fields'][$name]['image']['href'] = $href;
					return $this;
				}

			function AddProtectCode($name, $title='')
				{
					siman_generate_protect_code();
					$this->AddText($name, $title, true);
					$this->SetImage($name, 'ext/antibot/antibot.php?rand='.rand(11111, 99999));
					return $this;
				}

			function LabelAfterControl($name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->SetFieldEndText($name, ' '.$this->GetTitle($name));
					$this->SetTitleText($name, '');
					$this->MergeColumns($name);
					return $this;
				}

			function Disable($name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->form['fields'][$name]['attrs']['disabled'] = 'disabled';
					return $this;
				}

			function SetFieldClass($name = NULL, $classname='')
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->form['fields'][$name]['attrs']['class'] = $classname;
					return $this;
				}

			function AppendFieldClass($name = NULL, $classname='')
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->form['fields'][$name]['attrs']['class'] .= ' '.$classname;
					return $this;
				}

			function WithFieldClass($classname)
				{
					$this->SetFieldClass(NULL, $classname);
					return $this;
				}

			function WithFieldClassAppended($classname)
				{
					$this->AppendFieldClass(NULL, $classname);
					return $this;
				}

			function Readonly($name = NULL)
				{
					$this->SetFieldAttribute($name, 'readonly', 'readonly');
					return $this;
				}

			function Required($name = NULL, $value = true)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->form['fields'][$name]['required'] = $value;
					return $this;
				}

			function isRequired($name)
				{
					return !empty($this->form['fields'][$name]['required']);
				}

			function DisableAutofill($name=NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->SetFieldAttribute($name, 'autocomplete', 'off');
					return $this;
				}

			function DisableAutofillAllFields()
				{
					$this->disable_autofill_all_fields=true;
					return $this;
				}

		//-------------------------------------------------------------
			function SaveButton($text)
				{
					$this->form['savetitle'] = $text;
					return $this;
				}

			function SetSaveButtonHelperText($text)
				{
					$this->form['savebutton_helper']['text'] = $text;
					return $this;
				}

			function SetSaveButtonHelperClassname($class)
				{
					$this->form['savebutton_helper']['class'] = $class;
					return $this;
				}

			function DisableSubmitButton()
				{
					$this->form['nosubmitbutton'] = true;
				}

		//-------------------------------------------------------------
			function Calendar($name = NULL, $format='', $weekStart=NULL, $language = NULL)
				{
					sm_use('datepicker');
					if ($language === NULL)
						{
							if (sm_current_language() == 'ukr' || sm_current_language() == 'ua')
								$language = 'uk';
							else
								$language = 'en';
						}
					if ($language=='ukr' || $language=='ua')
						$language = 'uk';
					if (empty($format))
						{
							if ($language == 'uk')
								$format = 'dd.mm.yyyy';
							else
								$format = 'mm/dd/yyyy';
						}
					if ($weekStart === NULL)
						{
							if ($language == 'uk')
								$weekStart = 1;
							else
								$weekStart = 0;
						}
					if ($name === NULL)
						$name = $this->currentname;
					sm_html_headend('
							<script type="text/javascript">
							$(function()
								{
									$( "#'.$this->form['prefix'].$name.'" ).datepicker({format:"'.jsescape($format).'", autoclose: true, todayHighlight:true, weekStart:'.$weekStart.', language:"'.$language.'"});
								});
							</script>');
					$this->form['fields'][$name]['is_calendar'] = true;
					$this->form['fields'][$name]['calendar_format']=$format;
					return $this;
				}

			function WithCalendar($format='', $weekStart=NULL, $language = NULL)
				{
					return $this->Calendar(NULL, $format, $weekStart, $language);
				}

			function isSubtypeCalendar($name)
				{
					return $this->form['fields'][$name]['is_calendar']===true;
				}

			function WithMask($mask='', $placeholder='', $name = NULL)
				{
					sm_use('maskedinput');
					if ($name === NULL)
						$name = $this->currentname;
					sm_html_headend('
							<script type="text/javascript">
							$(function()
								{
									$( "#'.$this->form['prefix'].$name.'" ).mask("'.$mask.'"'.(sm_strlen($placeholder)>0?', {placeholder:"'.$placeholder.'"}':'').');
								});
							</script>');
					$this->form['fields'][$name]['is_maskedinput']=true;
					$this->form['fields'][$name]['maskedinput_mask']=$mask;
					return $this;
				}

			function isMaskedInput($name)
				{
					return $this->form['fields'][$name]['maskedinput_mask']===true;
				}

			function NoHighlight($turn_off = true)
				{
					if ($turn_off)
						$this->form['no_highlight'] = 1;
					else
						$this->form['no_highlight'] = 0;
					return $this;
				}

			function SetTooltipImage($name, $image = 'help.gif')
				{
					if (!file_exists(SM::ThemesPath(sm_current_theme().'/images/admintable/'.$image)))
						$this->form['fields'][$name]['tooltipimg'] = 'themes/default/images/admintable/'.$image;
					else
						$this->form['fields'][$name]['tooltipimg'] = 'themes/'.sm_current_theme().'/images/admintable/'.$image;
					$this->form['tooltip_present'] = true;
					return $this;
				}

			function Tooltip($name, $text, $image = 'help.gif')
				{
					$this->form['fields'][$name]['tooltip'] = $text;
					$this->SetTooltipImage($name, $image);
					$this->form['tooltip_present'] = true;
					return $this;
				}

			function WithTooltip($text, $image = 'help.gif', $name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->Tooltip($name, $text, $image);
					return $this;
				}

			function TooltipURL($name, $url, $open_in_new_page=true, $image = 'help.gif')
				{
					$this->form['fields'][$name]['tooltip_url'] = $url;
					if ($open_in_new_page)
						$this->form['fields'][$name]['tooltip_url_target'] = '_blank';
					$this->SetTooltipImage($name, $image);
					$this->form['tooltip_present'] = true;
					return $this;
				}

			function WithTooltipURL($url, $open_in_new_page=true, $image = 'help.gif', $name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					$this->TooltipURL($name, $url, $open_in_new_page, $image);
					return $this;
				}

		/**
		 * @deprecated
		 */
			function SendFieldsInfo()
				{
					return $this;
				}

			function Autocomplete($ajax_url, $name = NULL)
				{
					sm_use('autocomplete');
					sm_autocomplete_init_controls();
					if ($name === NULL)
						$name = $this->currentname;
					sm_autocomplete_for('#'.$this->form['prefix'].$name, $ajax_url);
					return $this;
				}

			function SetFocus($name = NULL)
				{
					if ($name === NULL)
						$name = $this->currentname;
					sm_setfocus($name);
					return $this;
				}

			function WithValue($value)
				{
					$this->SetValue($this->currentname, $value);
					return $this;
				}

			function WithNotEscapedValue($value)
				{
					$this->SetNotEscapedValue($this->currentname, $value);
					return $this;
				}

			function AddClassnameGlobal($classname)
				{
					$this->form['class'] .= ' '.$classname;
					return $this;
				}

			function SetStyleGlobal($style)
				{
					$this->SetFormAttribute('style', $style);
					return $this;
				}

			function javascriptCode($jscode)
				{
					$this->form['html_end'].='<script type="text/javascript">'.$jscode.'</script>';
				}

			public static function FieldTypeTagText()
				{
					return 'text';
				}

			public static function FieldTypeTagSeparator()
				{
					return 'separator';
				}

			public static function FieldTypeTagLabel()
				{
					return 'label';
				}

			public static function FieldTypeTagPassword()
				{
					return 'password';
				}

			public static function FieldTypeTagFile()
				{
					return 'file';
				}

			public static function FieldTypeTagStaticText()
				{
					return 'statictext';
				}

			public static function FieldTypeTagHidden()
				{
					return 'hidden';
				}

			public static function FieldTypeTagTextarea()
				{
					return 'textarea';
				}

			public static function FieldTypeTagSelect()
				{
					return 'select';
				}

			public static function FieldTypeTagCheckbox()
				{
					return 'checkbox';
				}

			public static function FieldTypeTagRadioGroup()
				{
					return 'radiogroup';
				}

			public static function FieldTypeTagWYSIWIGEditor()
				{
					return 'editor';
				}

			public static function FieldTypeTagPlainHTML()
				{
					return 'html';
				}

			public static function FieldTypeTagTPL()
				{
					return 'tpl';
				}

			function FieldsCount()
				{
					if (is_array($this->form['fields']))
						return count($this->form['fields']);
					else
						return 0;
				}
		}
