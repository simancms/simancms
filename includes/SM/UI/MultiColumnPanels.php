<?php

	namespace SM\UI;

	use SM\Common\Helpers\HTMLGenerator;
	use SM\Themes\CurrentTheme;
	use SM\UI\Interfaces\UIOutputTPLInterface;

	class MultiColumnPanels implements UIOutputTPLInterface
		{
			protected $panels=[];
			protected $columns_attrs=[];
			protected $current_index=0;
			protected $coll_class_map=[
				1=>'col-md-12',
				2=>'col-md-6',
				3=>'col-md-4',
				4=>'col-md-3',
			];
			protected $container_class='row';

			function __construct()
				{
					if (!CurrentTheme::GetConfigVar('ui.multicolumn.class_map')->isEmpty())
						$this->coll_class_map=CurrentTheme::GetConfigVar('ui.multicolumn.class_map')->AsArray();
					if (!CurrentTheme::GetConfigVar('ui.multicolumn.container_class')->isEmpty())
						$this->container_class=CurrentTheme::GetConfigVar('ui.multicolumn.container_class')->AsString();
					$this->AddColumn();
				}

		/**
		 * @return Panel
		 */
			function Current()
				{
					return $this->panels[$this->current_index];
				}

			function AddColumn()
				{
					$this->panels[]=new Panel();
					$this->columns_attrs[]=[
						'class'=>['sm-mcp-col'],
						'style'=>[],
					];
					$this->current_index=count($this->panels)-1;
					return $this->Current();
				}

			function AddColumnClass($class)
				{
					$this->columns_attrs[$this->current_index]['class'][]=$class;
				}

			private function ColCount()
				{
					return count($this->panels);
				}

			private function CollClass()
				{
					if (!isset($this->coll_class_map[$this->ColCount()]))
						return 'sm-mcp-default-col';
					return $this->coll_class_map[$this->ColCount()];
				}

			public function TPLDataArray()
				{
					$data['global_container']['start_html']='<div class="'.$this->container_class.'">';
					for ($i=0; $i<count($this->panels); $i++)
						{
							$attrs=$this->columns_attrs[$i];
							$attrs['class'][]=$this->CollClass();
							$data['panels'][$i]=[
								'panel'=>$this->panels[$i]->Output(),
								'container_start_html'=>'<div '.HTMLGenerator::AttributesFromArray($attrs).'>',
								'container_end_html'=>'</div>',
							];
						}
					$data['global_container']['end_html']='</div>';
					return $data;
				}

			public function TPLName()
				{
					return 'common_multicolumn_panels.tpl';
				}
		}

