<?php


	namespace SM\Core;

	abstract class ValueAccessPrototype
		{

			abstract protected function RawValue();

			function AsString()
				{
					return (string)$this->RawValue();
				}

			function AsStringOrDefault($default)
				{
					$str=$this->AsString();
					if (empty($str))
						return $default;
					else
						return $str;
				}

			function UrlencodedString()
				{
					return urlencode($this->AsString());
				}

			function EscapedString()
				{
					return htmlescape($this->AsString());
				}

			function isStringEqual($string_to_compare)
				{
					return strcmp($this->AsString(), $string_to_compare)===0;
				}

			function AsFloat()
				{
					return floatval($this->RawValue());
				}

			function AsInt()
				{
					return intval($this->RawValue());
				}

			function AsBool()
				{
					return $this->AsInt()>0;
				}

			function AsAbsInt()
				{
					return abs($this->AsInt());
				}

			function AsArray()
				{
					if ($this->RawValue()===NULL)
						return [];
					elseif (!is_array($this->RawValue()))
						return [$this->RawValue()];
					else
						return $this->RawValue();
				}

			abstract function SetValue($new_value);

			function isEmpty()
				{
					return empty($this->RawValue());
				}

		}
