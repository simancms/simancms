<?php

	namespace SM\Core;

	use SM\SM;

	class SMURL
		{

			public static function Home()
				{
					return sm_homepage();
				}

			public static function AdminControlPanel()
				{
					return 'index.php?m=admin';
				}

			public static function AdminModulesManagement()
				{
					return 'index.php?m=admin&d=modules';
				}

			public static function MyAccount()
				{
					if (!SM::Settings('cabinet_module')->isEmpty())
						return sm_fs_url('index.php?m='.urlencode(SM::Settings('cabinet_module')->AsString()));
					else
						return sm_fs_url('index.php?m=account&d=cabinet');
				}

		}
