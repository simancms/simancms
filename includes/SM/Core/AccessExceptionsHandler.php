<?php


	namespace SM\Core;

	use SM\Access\SMAccessDeniedException;
	use SM\Common\Redirect;

	class AccessExceptionsHandler
		{

			public static function HandleException($sm_access_denied_exception)
				{
					if (!is_a($sm_access_denied_exception, SMAccessDeniedException::class))
						return;
					if ($sm_access_denied_exception->error404)
						{
							sm_title(sm_lang('error404.title'));
							sm_template('404');
						}
					elseif ($sm_access_denied_exception->enforce_login)
						Redirect::Now(sm_fs_url('index.php?m=account&d=login'));
					else
						sm_access_denied();
				}

		}
