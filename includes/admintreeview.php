<?php

	//==============================================================================
	//#revision 2020-09-20
	//==============================================================================

	if (!defined("admintreeview_DEFINED"))
		{
			/** @deprecated  */
			class TTreeView extends \SM\UI\TreeView
				{
				}

			define("admintreeview_DEFINED", 1);
		}
