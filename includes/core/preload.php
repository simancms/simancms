<?php

	//------------------------------------------------------------------------------
	//|            Content Management System SiMan CMS                             |
	//|              http://simancms.apserver.org.ua                               |
	//------------------------------------------------------------------------------

	//Preload modules begin
	use SM\SM;

	sm_event('beforepreload', Array());
	$autoloadmodules = nllistToArray(sm_settings('autoload_modules'));
	for ($autoloadmodulesindex = 0; $autoloadmodulesindex < count($autoloadmodules); $autoloadmodulesindex++)
		{
			if (sm_strpos($autoloadmodules[$autoloadmodulesindex], ':')!==false || sm_strpos($autoloadmodules[$autoloadmodulesindex], '.')!==false || strpos($autoloadmodules[$autoloadmodulesindex], '/')!==false || strpos($autoloadmodules[$autoloadmodulesindex], '\\')!==false || empty($autoloadmodules[$autoloadmodulesindex]))
				continue;
			if (file_exists(SM::ModulesPath('preload/'.$autoloadmodules[$autoloadmodulesindex].'.php')))
				include_once(SM::ModulesPath('preload/'.$autoloadmodules[$autoloadmodulesindex].'.php'));
			if (SM::isLoggedIn())
				{
					if (SM::User()->Level()>=1 && file_exists(SM::ModulesPath('preload/level1/'.$autoloadmodules[$autoloadmodulesindex].'.php')))
						include_once(SM::ModulesPath('preload/level1/'.$autoloadmodules[$autoloadmodulesindex].'.php'));
					if (SM::User()->Level()>=2 && file_exists(SM::ModulesPath('preload/level2/'.$autoloadmodules[$autoloadmodulesindex].'.php')))
						include_once(SM::ModulesPath('preload/level2/'.$autoloadmodules[$autoloadmodulesindex].'.php'));
					if (SM::User()->Level()>=3 && file_exists(SM::ModulesPath('preload/level3/'.$autoloadmodules[$autoloadmodulesindex].'.php')))
						include_once(SM::ModulesPath('preload/level3/'.$autoloadmodules[$autoloadmodulesindex].'.php'));
				}
		}
	sm_event('afterpreload', Array());
	//Preload modules end

